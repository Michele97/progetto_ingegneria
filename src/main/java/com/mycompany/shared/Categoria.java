package com.mycompany.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Categoria implements IsSerializable,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nome;

	public Categoria(String nome){
		this.nome=nome;
	}

	public Categoria() {
	}

	public String getNome(){
		return this.nome;
	}
	public void setNome(String name){
		this.nome=name;
	}
}
