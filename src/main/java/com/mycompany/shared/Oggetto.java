package com.mycompany.shared;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.ibm.icu.text.SimpleDateFormat;
import com.google.gwt.i18n.client.DateTimeFormat;
public class Oggetto implements IsSerializable, Serializable{


	/**
	 *
	 */
	private static final long serialVersionUID = -7707235251524756637L;

	private String proprietario;
	private String categoria;
	private String nome;
	private Double prezzo;
	private String descrizione;
	private String dataPubblicazione;
	private String dataScadenza;
	private String id;
	private String stato;

	//creo la blasse OggettoBuilder per istanziare l'oggetto con il pattern builder
	public static class OggettoBuilder{

		private String proprietario;
		private String categoria;
		private String nome;
		private String dataPubblicazione;
		private String dataScadenza;
		private Double prezzo;
		private String descrizione;
		private String id;
		private String stato;

		//	public OggettoBuilder(){}
		public OggettoBuilder(String proprietario, String categoria, String nome, String dataPubblicazione, String dataScadenza, Double prezzo, String descrizione) {
			this.proprietario = proprietario;
			this.categoria = categoria;
			this.nome = nome;
			this.dataPubblicazione = dataPubblicazione;
			this.dataScadenza = dataScadenza;
			this.prezzo = prezzo;
			this.descrizione = descrizione;
		}
		public OggettoBuilder setId(String id) {
			this.id=id;
			return this;
		}

		public OggettoBuilder setStato(String stato) {
			this.stato=stato;
			return this;
		}

		public Oggetto build() {
			// call the private constructor in the outer class
			return new Oggetto(this);
		}

	}

	private Oggetto(OggettoBuilder builder) {
		this.proprietario = builder.proprietario;
		this.categoria = builder.categoria;
		this.nome = builder.nome;
		this.dataPubblicazione = builder.dataPubblicazione;
		this.dataScadenza = builder.dataScadenza;
		this.prezzo = builder.prezzo;
		this.descrizione = builder.descrizione;
		this.id=builder.id;
		this.stato=builder.stato;
	}

	public Oggetto() {
	}

	public void setStato(String stato) {
		this.stato=stato;
	}

	public String getId() {
		return this.id;
	}

	public String getNome() {
		return this.nome;
	}

	public String getCategoria() {
		return this.categoria;
	}
	public String getProprietario() {
		return this.proprietario;
	}

	public String getDataPubblicazione() {
		return this.dataPubblicazione;
	}
	public String getDataScadenza() {
		return this.dataScadenza;
	}
	public Double getPrezzo() {
		return this.prezzo;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public String getStato() {
		return this.stato;
	}


	@Override
	public boolean equals(Object ogg) {
		return (this.getId().equals(((Oggetto) ogg).getId()));
	}
}


