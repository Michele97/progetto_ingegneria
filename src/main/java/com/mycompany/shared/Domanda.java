package com.mycompany.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Domanda implements IsSerializable, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Oggetto oggetto;
	private String question;
	private String mittente;
	private String id;

	public Domanda(Oggetto ogg, String question, String mittente, String id){

		this.oggetto=ogg;
		this.question=question;
		this.mittente=mittente;
		this.id=id;
	}
	public Domanda(){}

	public Oggetto getOggettoDomanda(){
		return this.oggetto;
	}

	public String getQuestion(){
		return this.question;
	}

	public String getMittente(){
		return this.mittente;
	}
	
	public String getIdDomanda(){
		return this.id;
	}
	@Override
	public boolean equals(Object ogg) {
		return (this.getIdDomanda().equals(((Domanda) ogg).getIdDomanda()));
	}

}
