package com.mycompany.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SottoCategoria extends Categoria implements Serializable, IsSerializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String categoriaMadre;
	public SottoCategoria() {

	}
	public SottoCategoria(String nomeCategoria, String categoriaMadre){

		super(nomeCategoria);
		this.categoriaMadre=categoriaMadre;
	}
	public String getCategoriaMadre(){
		return this.categoriaMadre;
	}
	/*public String getNome(){
    return nomeCategoria;
  }*/
	public void setNome(String name){
		this.categoriaMadre=name;
	}
}
