package com.mycompany.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Utente implements IsSerializable, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String nome;
	private String cognome;
	private String telefono;
	private String email;
	private String codiceFiscale;
	private String indirizzo;
	private String sesso;
	private String dataNascita;
	private String luogoNascita;

	public static class UtenteBuilder{

		private String username;
		private String password;
		private String nome;
		private String cognome;
		private String telefono;
		private String email;
		private String codiceFiscale;
		private String indirizzo;
		private String sesso;
		private String dataNascita;
		private String luogoNascita;

		//creo la blasse UtenteBuilder per istanziare l'utente con il pattern builder
		public UtenteBuilder(String username, String password, String nome, String cognome, String telefono, String email, String codiceFiscale, String indirizzo){

			this.username = username;
			this.password = password;
			this.nome = nome;
			this.cognome = cognome;
			this.telefono = telefono;
			this.email = email;
			this.codiceFiscale = codiceFiscale;
			this.indirizzo = indirizzo;
		}

		public UtenteBuilder setSesso(String sesso) {
			this.sesso = sesso;
			return this;
		}
		public UtenteBuilder setDataNascita(String dataNascita) {
			this.dataNascita = dataNascita;
			return this;
		}
		public UtenteBuilder setLuogoNascita(String luogoNascita) {
			this.luogoNascita = luogoNascita;
			return this;
		}
		public Utente build() {
			return new Utente(this);
		}

	}

	private Utente(UtenteBuilder user){

		this.username = user.username;
		this.password = user.password;
		this.nome = user.nome;
		this.cognome = user.cognome;
		this.telefono = user.telefono;
		this.email = user.email;
		this.codiceFiscale = user.codiceFiscale;
		this.indirizzo = user.indirizzo;
		this.sesso = user.sesso;
		this.dataNascita = user.dataNascita;
		this.luogoNascita = user.luogoNascita;

	}
	
	public Utente() {
		
	}



	public String getUsername() {
		return this.username;
	}
	public String getPassword() {
		return this.password;
	}
	public String getNome() {
		return this.nome;
	}
	public String getCognome() {
		return this.cognome;
	}
	public String getTelefono() {
		return this.telefono;
	}
	public String getEmail() {
		return this.email;
	}
	public String getCodiceFiscale() {
		return this.codiceFiscale;
	}
	public String getIndirizzo() {
		return this.indirizzo;
	}
	public String getSesso() {
		return this.sesso;
	}
	public String getDataNascita() {
		return this.dataNascita;
	}
	public String getLuogoNascita() {
		return this.luogoNascita;
	}
}
