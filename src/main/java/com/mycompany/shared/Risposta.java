package com.mycompany.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Risposta implements IsSerializable, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String answer;
	public Domanda question;
	private String id;
	

	public Risposta(String answer, Domanda question, String id){

		this.question=question;
		this.answer=answer;
		this.id=id;
		
	}

	public Risposta(){}

	public String getAnswer(){
		return this.answer;
	}

	public Domanda getInstanceDomanda(){
		return this.question;
	}
	
	public String getIdRisposta(){
		return this.id;
	}
	
	@Override
	public boolean equals(Object ogg) {
		return (this.getIdRisposta().equals(((Risposta) ogg).getIdRisposta()));
	}

}
