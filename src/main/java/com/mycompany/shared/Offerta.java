package com.mycompany.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.mycompany.server.serializerobjectprog.SerializerOggetto;

public class Offerta implements IsSerializable, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String offerente;
	private Double importo;
	public Oggetto oggetto;
	private String stato;
	private String id;
	//aggiungere campo data
	public Offerta(String offerente, Double importo,Oggetto oggetto, String stato, String id){

		this.offerente=offerente;
		this.importo=importo;
		this.oggetto=oggetto;
		this.stato=stato;
		this.id=id;

	}

	public Offerta(){}

	public String getOfferente(){
		return this.offerente;
	}

	public String getStato(){
		return this.stato;
	}
	public Double getImporto(){
		return this.importo;
	}
	public Oggetto getOggetto(){
		return this.oggetto;
	}
	public void setImporto(Double importo){
		this.importo=importo;
	}
	public void setOfferente(String offerente){
		this.offerente=offerente;
	}
	public void setStato(String stato){
		this.stato=stato;
	}
	
	public String getIdOfferta(){
		return this.id;
	}
	
	@Override
	public boolean equals(Object ogg) {
		return (this.getIdOfferta().equals(((Offerta) ogg).getIdOfferta()));
	}

}
