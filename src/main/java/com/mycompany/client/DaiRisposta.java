package com.mycompany.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mycompany.shared.Domanda;

public class DaiRisposta {
	private static final String OBJECT_ERROR = "No object found.";
	
	String username;
	SchermataUtente su;
	GreetingServiceAsync gtasync;
	
	public DaiRisposta(String username, SchermataUtente su, GreetingServiceAsync gtasync) {
		this.username=username;
		this.su=su;
		this.gtasync=gtasync;
	}
	
	int counter=0;
	public void viewDomande(ArrayList<Domanda> domanda){

		RootPanel.get().clear();

		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("300");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		final VerticalPanel foundObjectsPanel = new VerticalPanel();
		//HTML nomeOggetto=new HTML("Nome oggetto trovato: "+domanda.get(0).getNome());
		int i = 0;
		for (i = 0; i < domanda.size();i++)	{

			final int counter = i;

			//if(domanda.get(counter).getDestinatario().equals(username))

			final HorizontalPanel answerPanel = new HorizontalPanel();
			final TextBox answerTextBox = new TextBox();
			answerTextBox.getElement().setPropertyString("placeholder", "Rispondi");
			answerPanel.add(answerTextBox);

			final Button risposta = new Button("Rispondi");
			risposta.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent arg0) {
					sendRispondi(domanda.get(counter), answerTextBox.getText());
				}
			});



			HTML domande=new HTML("<p><br><br></p>"+
					"<br>"+"Nome oggetto: "+domanda.get(i).getOggettoDomanda().getNome()+"</br>"+
					"<br>"+"Mittente: "+domanda.get(i).getMittente()+"</br>"+
					"<br>"+"Domanda: "+domanda.get(i).getQuestion()+"</br>");
			foundObjectsPanel.add(domande);
			formPanel.add(foundObjectsPanel);

			foundObjectsPanel.add(answerPanel);
			formPanel.add(foundObjectsPanel);

			foundObjectsPanel.add(risposta);
			formPanel.add(foundObjectsPanel);


		}

		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Domande ricevute");
		boxInsertObject.setAnimationEnabled(true);
		boxInsertObject.add(formPanel);
		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");


		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				if(username != null) {
					su.showSchermata();
				}else {
					Window.alert("Prima devi registrarti alla piattaforma");
				}
			}

		});



		final HorizontalPanel buttonsSellExit = new HorizontalPanel();

		buttonsSellExit.add(esci);
		boxInsertObject.setWidget(formPanel);
		formPanel.add(buttonsSellExit);
		RootPanel.get().add(boxInsertObject);
	}


	private void sendRispondi(Domanda d, String risposta){

		gtasync.rispondi(d,risposta, new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			public void onSuccess(String result) {

				Window.alert(result);
				su.showSchermata();
			}
		});
	}
	
	public void sendCercaDomande(String username) {
	
		gtasync.cercaDomande(username, new AsyncCallback<ArrayList<Domanda>>() {
			
			public void onFailure(Throwable caught) {
				
				Window.alert(caught.getMessage());
			}
			
			public void onSuccess(ArrayList<Domanda> result) {
				
				//se l'arraylist che mi ritorna è vuoto, allora non ho trovato l'oggetto
				if(result.isEmpty()) {
					Window.alert(OBJECT_ERROR);
					su.showSchermata();
				}else { //passo l'array list contenente gli oggetti al metodo che li mostrerà
					RootPanel.get().clear();
					viewDomande(result);
				}

			}
		});
	}

}
