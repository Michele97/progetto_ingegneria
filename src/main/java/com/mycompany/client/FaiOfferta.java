package com.mycompany.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mycompany.shared.Oggetto;

public class FaiOfferta {
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	private static final String PRICE_ERROR = "Not a valid price.";
	private String username;
	private SchermataUtente indexProfilo;
	private GreetingServiceAsync gtasync;
	public FaiOfferta(String username, SchermataUtente index, GreetingServiceAsync gtasync) {
		this.username = username;
		this.indexProfilo = index;
		this.gtasync = gtasync;
	}

	//gli passo l'oggetto con tutti i suoi parametri, contenuto nell'arraylist
	public void mostraSchermataOfferta(Oggetto oggettoTrovato) {

		RootPanel.get().clear();
		String name = this.username;
		HorizontalPanel hpInsOgg=new HorizontalPanel();
		VerticalPanel vpwelcome=new VerticalPanel();
		vpwelcome.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);

		Button tornaIndietro = new Button("Torna indietro");

		tornaIndietro.setEnabled(true);

		HTML welcome = new HTML("<p style=\"font-size:18px; font-family:Helvetica;\"><b>Ciao " + this.username + "!</b><br> Fai la tua offerta!</p>");
		hpInsOgg.add(tornaIndietro);
		vpwelcome.setWidth("100%");
		vpwelcome.add(welcome);
		vpwelcome.add(hpInsOgg);
		RootPanel.get().add(vpwelcome);

		final HorizontalPanel ownerPanel = new HorizontalPanel();
		HTML user=new HTML("Fai la tua offerta "+this.username+" !");
		ownerPanel.add(user);

		final HorizontalPanel nameObjectPanel = new HorizontalPanel();
		HTML nameOfObject=new HTML("Nome oggetto: "+oggettoTrovato.getNome());
		nameObjectPanel.add(nameOfObject);

		final HorizontalPanel actualPricePanel = new HorizontalPanel();
		HTML actualPrice=new HTML("Prezzo di partenza: "+oggettoTrovato.getPrezzo());
		actualPricePanel.add(actualPrice);

		final HorizontalPanel maxPricePanel = new HorizontalPanel();
		double prezzoMaggiore = oggettoTrovato.getPrezzo();

		//aggiorno il prezzo più alto 	
		gtasync.maxPrezzo(oggettoTrovato.getId(), new AsyncCallback<Double>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(Double result) {
				final double prezzoMax = result;
				if(result < oggettoTrovato.getPrezzo()) {
					HTML maxPrice=new HTML("Offerta più alta: "+oggettoTrovato.getPrezzo());
					maxPricePanel.add(maxPrice);
				}else {
					HTML maxPrice=new HTML("Offerta più alta: "+prezzoMax);
					maxPricePanel.add(maxPrice);
				}

			}

		});

		final VerticalPanel pricePanelOffered = new VerticalPanel();
		HTML priceHtml = new HTML("Che prezzo vuoi offrire? ");
		final TextBox priceTextBox = new TextBox();
		priceTextBox.getElement().setPropertyString("placeholder", "Prezzo che vuoi offrire");
		pricePanelOffered.add(priceHtml);
		pricePanelOffered.add(priceTextBox);

		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("100%");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		formPanel.add(user);
		formPanel.add(nameObjectPanel);
		formPanel.add(actualPricePanel);
		formPanel.add(maxPricePanel);
		formPanel.add(pricePanelOffered);

		final HorizontalPanel buttonsSendOffer = new HorizontalPanel();
		final Button sendOffer = new Button("Invia offerta");
		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Fai la tua offerta");
		boxInsertObject.setAnimationEnabled(true);

		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");

		buttonsSendOffer.add(sendOffer);
		formPanel.add(buttonsSendOffer);
		boxInsertObject.setWidget(formPanel);
		RootPanel.get().add(boxInsertObject);

		sendOffer.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				//questi due parametri prezzoOff e prezzoPartenza dovevano essere final
				if (checkPrice(Math.abs(Double.parseDouble(priceTextBox.getText())), oggettoTrovato.getPrezzo())) {
					sendFaiOfferta(name, oggettoTrovato,Math.abs(Double.parseDouble(priceTextBox.getText())));
					//ricarico la schermata offerta
					mostraSchermataOfferta(oggettoTrovato);
				}else { //se il controllo non è passato allora stampo un messaggio d'errore
					Window.alert(PRICE_ERROR);
				}
			}
		});

		tornaIndietro.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				indexProfilo.showSchermata();
			}
		});
	}
	//questo metodo prende come parametri in input:
	// l'oggetto e il prezzo offerto.
	public void sendFaiOfferta(String offerente,Oggetto oggettoDellOfferta, Double prezzoOfferto) {
		//dal parametro OGGETTODELLOFFERTA tirerò fuori l'id dell oggetto e lo passero al server
		gtasync.faiOfferta(offerente, prezzoOfferto, oggettoDellOfferta, new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			public void onSuccess(String result) {
				if (result.equals("ok")) {
					Window.alert("Ora sei l'offerente che ha offerto la cifra più alta!");
				} else {
					Window.alert("Il denaro offerto non è maggiore di quello offerto dal vecchio offerente.");
				}
			}
		});
	}
	//metodo che controlla se il prezzo offerto è maggiore del prezzo di partenza o dell ultimo prezzo offerto a quell oggetto
	public boolean checkPrice(double prezzoOfferto, double prezzoDiPartenza) {
		if(prezzoOfferto<=prezzoDiPartenza) {
			return false;
		}else{
			return true;
		}
	}


}
