package com.mycompany.client;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mycompany.shared.Utente;

public class UserInfo {

	private String username;
	GreetingServiceAsync gtasync;

	public UserInfo(String username,GreetingServiceAsync gtasync) {
		this.username=username;
		this.gtasync=gtasync;
	}

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	public void sendShowUserInfo(String username) {
		gtasync.showUserInfo(username, new AsyncCallback<Utente>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(Utente u) {
				showUserInfoClient(u);
			}
		});
	}

	private void showUserInfoClient(Utente u) {
		VerticalPanel userInfoPanel = new VerticalPanel();
		userInfoPanel.setHeight("100%");
		userInfoPanel.setWidth("100%");
		userInfoPanel.setSpacing(10);
		userInfoPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HorizontalPanel userPanel=new HorizontalPanel();

		HTML userView=new HTML("Username: "+u.getUsername()+"<br>");
		userPanel.add(userView);
		userInfoPanel.add(userPanel);

		HorizontalPanel namePanel=new HorizontalPanel();
		HTML nameView=new HTML("Nome: "+u.getNome()+"<br>");
		namePanel.add(nameView);
		userInfoPanel.add(namePanel);

		HorizontalPanel surnamePanel=new HorizontalPanel();
		HTML surnameView=new HTML("Cognome: "+u.getCognome()+"<br>");
		surnamePanel.add(surnameView);
		userInfoPanel.add(surnamePanel);

		HorizontalPanel phonePanel=new HorizontalPanel();
		HTML phoneView=new HTML("Telefono: "+u.getTelefono()+"<br>");
		phonePanel.add(phoneView);
		userInfoPanel.add(phonePanel);
		
		HorizontalPanel emailPanel=new HorizontalPanel();
		HTML emailView=new HTML("Email: "+u.getEmail()+"<br>");
		emailPanel.add(emailView);
		userInfoPanel.add(emailPanel);
		HorizontalPanel cfPanel=new HorizontalPanel();
		HTML cfView=new HTML("Codice Fiscale: "+u.getCodiceFiscale()+"<br>");
		cfPanel.add(cfView);
		userInfoPanel.add(cfPanel);
		HorizontalPanel addressPanel=new HorizontalPanel();
		HTML addressView=new HTML("Indirizzo: "+u.getIndirizzo()+"<br>");
		addressPanel.add(addressView);
		userInfoPanel.add(addressPanel);
		if(!u.getSesso().contentEquals("")) {
			HorizontalPanel sexPanel=new HorizontalPanel();
			HTML sexView=new HTML("Sesso: "+u.getSesso()+"<br>");
			sexPanel.add(sexView);
			userInfoPanel.add(sexPanel);
		}
		if(!u.getDataNascita().contentEquals("noData")) {
			HorizontalPanel dateNPanel=new HorizontalPanel();
			HTML dateNView=new HTML("Data di nascita: "+u.getDataNascita()+"<br>");
			dateNPanel.add(dateNView);
			userInfoPanel.add(dateNPanel);
		}
		if(!u.getLuogoNascita().contentEquals("")) {
			HorizontalPanel luogoNPanel=new HorizontalPanel();
			HTML luogoNView=new HTML("Luogo di nascita: "+u.getLuogoNascita()+"<br>");
			luogoNPanel.add(luogoNView);
			userInfoPanel.add(luogoNPanel);
		}
		
		userInfoPanel.setHeight("100");
		userInfoPanel.setWidth("300");
		userInfoPanel.setSpacing(10);
		userInfoPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

		DialogBox userInfoBox = new DialogBox();
		userInfoBox.setText("Le tue info");
		userInfoBox.setAnimationEnabled(true);
		userInfoBox.add(userInfoPanel);
		userInfoBox.addStyleName("userinfobox");
		userInfoBox.setWidth("100%");
		RootPanel.get().add(userInfoBox);

	}
}
