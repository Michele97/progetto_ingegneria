package com.mycompany.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mycompany.shared.Oggetto;

public class FaiDomanda {


	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	//settare l'username a sconosciuto
	private String username;
	private SchermataUtente indexProfilo;
	private GreetingServiceAsync gtasync;
	//servirà per richiamare questa classe anche dalla classe HOME MYMODULE
	//private ByeBay index;
	public FaiDomanda(String username, SchermataUtente index, GreetingServiceAsync gtasync) {
		this.username = username;
		this.indexProfilo = index;
		this.gtasync = gtasync;
	}



	public void mostraSchermataDomanda(Oggetto oggettoTrovato) {

		RootPanel.get().clear();
		String name = this.username;
		HorizontalPanel hpInsOgg=new HorizontalPanel();
		VerticalPanel vpwelcome=new VerticalPanel();
		vpwelcome.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);


		HTML welcome = new HTML("<p style=\"font-size:18px; font-family:Helvetica;\"><b>Ciao " + this.username + "!</b><br> Fai la tua domanda!</p>");
		vpwelcome.setWidth("100%");
		vpwelcome.add(welcome);
		vpwelcome.add(hpInsOgg);
		RootPanel.get().add(vpwelcome);


		final HorizontalPanel ownerPanel = new HorizontalPanel();
		HTML user=new HTML("Fai la tua domanda a "+oggettoTrovato.getProprietario()+" !");
		ownerPanel.add(user);

		final HorizontalPanel nameObjectPanel = new HorizontalPanel();
		HTML nameOfObject=new HTML("Nome oggetto: "+oggettoTrovato.getNome());
		nameObjectPanel.add(nameOfObject);

		final VerticalPanel pricePanelOffered = new VerticalPanel();
		HTML priceHtml = new HTML("Domanda: ");
		final TextBox priceTextBox = new TextBox();
		priceTextBox.getElement().setPropertyString("placeholder", "Testo...");
		pricePanelOffered.add(priceHtml);
		pricePanelOffered.add(priceTextBox);


		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("300");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		formPanel.add(user);
		formPanel.add(nameObjectPanel);
		//	formPanel.add(actualPricePanel);
		formPanel.add(pricePanelOffered);

		final HorizontalPanel buttonsSendOrTurnBack = new HorizontalPanel();
		Button sendOffer = new Button("Invia domanda");
		Button tornaIndietro = new Button("Torna indietro");
		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Fai la tua domanda");
		boxInsertObject.setAnimationEnabled(true);

		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");

		buttonsSendOrTurnBack.add(sendOffer);
		buttonsSendOrTurnBack.add(tornaIndietro);
		formPanel.add(buttonsSendOrTurnBack);
		boxInsertObject.setWidget(formPanel);
		RootPanel.get().add(boxInsertObject);

		sendOffer.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				if(name!="sconosciuto"){
					sendFaiDomanda(oggettoTrovato, priceTextBox.getText(), name);
				}else{
					Window.alert("Devi registrarti alla piattaforma!");
				}
			}
		});

		tornaIndietro.setEnabled(true);
		tornaIndietro.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				indexProfilo.showSchermata();
			}
		});
	}



	private void sendFaiDomanda(Oggetto o, String question, String mittente){

		gtasync.faiDomanda(o, question, mittente, new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(String result) {
				Window.alert(result);
				indexProfilo.showSchermata();
			}
		});

	}
}
