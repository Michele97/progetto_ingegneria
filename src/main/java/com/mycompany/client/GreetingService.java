package com.mycompany.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.mycompany.shared.*;


/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	
	String register(String username, String password, String nome, String cognome, String telefono, String email, String codiceFiscale,
			String indirizzo, String sesso, String dataNascita, String luogoNascita) throws IllegalArgumentException;
	String login(String username, String password) throws IllegalArgumentException;
	String vendiOggetto(String proprietario, String categoria, String nome, String dataPubblicazione, String dataScadenza, Double prezzo, String descrizione) throws IllegalArgumentException;
	ArrayList<Oggetto> cercaOggetto(String nomeOggetto) throws IllegalArgumentException;
	void creaCategorie() throws IllegalArgumentException;
	String faiOfferta(String offerente, Double prezzoOfferto, Oggetto oggetto) throws IllegalArgumentException;
	Double maxPrezzo(String idOggetto) throws IllegalArgumentException;
	String faiDomanda(Oggetto ogg, String question, String mittente) throws IllegalArgumentException;
	String rispondi(Domanda d, String risposta/*int idOggetto, String nomeOggetto, String question, String answer, String mittente, String destinatario*/) throws IllegalArgumentException;
	ArrayList<Domanda> cercaDomande(String username) throws IllegalArgumentException;
	ArrayList<Risposta> cercaRisposte(String username) throws IllegalArgumentException;
	ArrayList<String> domandeRisposte(Oggetto ogg) throws IllegalArgumentException;
	Utente showUserInfo(String username) throws IllegalArgumentException;
	ArrayList<Oggetto> showAllObjects() throws IllegalArgumentException;
	ArrayList<Offerta> cercaOfferte(String username) throws IllegalArgumentException;
	ArrayList<Oggetto> cercaTuoiOggetti(String username) throws IllegalArgumentException;
	ArrayList<Categoria> cercaCategoria() throws IllegalArgumentException;
	ArrayList<SottoCategoria> cercaSottoCategoria() throws IllegalArgumentException;
	String insertCategory(String nomeCategoria) throws IllegalArgumentException;
	String insertSubCategory(String nomeCategoria, String categoriaMadre) throws IllegalArgumentException;
	String renameCategory(String oldName, String newName) throws IllegalArgumentException;
	String deleteOffert(Offerta o) throws IllegalArgumentException;
	ArrayList<Offerta> allOffert() throws IllegalArgumentException;
	String removeObject(Oggetto ogg) throws IllegalArgumentException;
	ArrayList<Oggetto> allObjects() throws IllegalArgumentException;
	ArrayList<Risposta> allAnswers() throws IllegalArgumentException;
	String removeRisposta(Risposta r) throws IllegalArgumentException;
	ArrayList<Domanda> allQuestions() throws IllegalArgumentException;
	String removeDomanda(Domanda d) throws IllegalArgumentException;
	
}
