package com.mycompany.client;



import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.mycompany.shared.*;


public class ByeBay implements EntryPoint {

	//errore nel caso in cui il server non risponda
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	//creo l'oggetto async
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	//creo l'oggetto messages per i bottoni
	private final Messages messages = GWT.create(Messages.class);
	public CercaOggetto cercaOggettoInPiattaforma;
	private String username = "sconosciuto";

	//metodo dell'entry point per creare la schermata iniziale
	public void onModuleLoad() {
		//greetingService.test();
		RootPanel.get().clear();
		//passo a cercaOggettoInPiattaforma l'username dell'utente e questa classe
		cercaOggettoInPiattaforma = new CercaOggetto(this.username, this, greetingService);
		//classe che contiente i metodi per visualizzare tutti gli oggetti
		ObjectInfo oi = new ObjectInfo(this.username,this,greetingService);
		//istanzio i bottoni di accesso e registrazione
		final Button loginButton = new Button( messages.loginButton() );
		final Button registerButton = new Button( messages.registerButton() );
		Button searchOggetto = new Button("Cerca un oggetto");

		//assegno lo stile ai bottoni
		loginButton.addStyleName("sendButton");
		registerButton.addStyleName("sendButton");
		searchOggetto.addStyleName("sendButton");

		//invoco il metodo clear per ripulire il pannello
		clear();
		//aggiungo i bottoni alla schermata iniziale
		RootPanel.get("loginButton").add(loginButton);
		RootPanel.get("registerButton").add(registerButton);
		RootPanel.get("searchButton").add(searchOggetto);
		loginButton.setEnabled(true);
		registerButton.setEnabled(true);
		searchOggetto.setEnabled(true);


		//click handler per il login
		loginButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				searchOggetto.setEnabled(true);
				loginButton.setEnabled(false);
				registerButton.setEnabled(false);
				//richiamo il metodo per far vedere il modulo di login
				showSchermataLogin();
			}
		});

		//click handler per la registrazione
		registerButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//disabilito i bottoni nel frattempo che il modulo di registrazione è aperto
				searchOggetto.setEnabled(false);
				loginButton.setEnabled(false);
				registerButton.setEnabled(false);
				//richiamo il metodo per far vedere il modulo di registrazione
				showSchermataRegister();
			}
		});


		searchOggetto.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				loginButton.setEnabled(false);
				registerButton.setEnabled(false);
				searchOggetto.setEnabled(false);
				//istanziare una classe di CercaOggetto
				cercaOggettoInPiattaforma.cercaOggetto();
			}

		});
		// --------------------------- WORKING ---------------------------
		//stampo tutti gli oggetti presenti nel Database in ordine cronologico di scadenza
		//deve essere implementato in GreetinServiceImpl
		RootPanel.get().add(oi.visualizzaTuttiGliOggetti());
		

	}
	//questo metodo deve rimanere public perchè verrà richiamato da SchermataUtente


	//metodo per stampare (nella home page) tutti gli oggetti presenti nella piattaforma
	

	//metodo per ripulire il pannello
	public void clear() {
		RootPanel.get("loginButton").clear();
		RootPanel.get("registerButton").clear();
		RootPanel.get("searchButton").clear();
		RootPanel rpL = RootPanel.get("boxLogin");
		RootPanel rpR = RootPanel.get("boxRegister");
		if (rpR != null) {
			rpR.clear();
		}
		if (rpL != null) {
			rpL.clear();
		}
	}

	//metodo per mostrare il modulo di registrazione
	private void showSchermataRegister() {

		//di seguito setto tutti i campi che poi andrò ad inserire nel dialogbox
		final HorizontalPanel usernamePanel = new HorizontalPanel();
		HTML usernameLabel = new HTML("Username: "); 
		final TextBox usernameTextBox = new TextBox();
		usernameTextBox.getElement().setPropertyString("placeholder", "Username");
		usernamePanel.add(usernameTextBox);

		final HorizontalPanel passwordPanel = new HorizontalPanel();
		HTML passwordLabel = new HTML("<br>Password: "); 
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		passwordTextBox.getElement().setPropertyString("placeholder", "*******");
		passwordPanel.add(passwordLabel);
		passwordPanel.add(passwordTextBox);
		//vanno aggiunti tutti gli altri campi

		final HorizontalPanel nomePanel = new HorizontalPanel();
		HTML nameLabel = new HTML("<br>Nome: "); 
		final TextBox nomeTextBox = new TextBox();
		nomeTextBox.getElement().setPropertyString("placeholder", "Nome");
		nomePanel.add(nomeTextBox);

		final HorizontalPanel cognomePanel = new HorizontalPanel();
		HTML cognomeLabel = new HTML("<br>Cognome: "); 
		final TextBox cognomeTextBox = new TextBox();
		cognomeTextBox.getElement().setPropertyString("placeholder", "Cognome");
		cognomePanel.add(cognomeTextBox);
		

		final HorizontalPanel telefonoPanel = new HorizontalPanel();
		HTML telefonoLabel = new HTML("<br>Telefono: "); 
		final TextBox telefonoTextBox = new TextBox();
		telefonoTextBox.getElement().setPropertyString("placeholder", "Telefono");
		telefonoPanel.add(telefonoTextBox);

		final HorizontalPanel emailPanel = new HorizontalPanel();
		HTML emailLabel = new HTML("<br>Email: "); 
		final TextBox emailTextBox = new TextBox();
		emailTextBox.getElement().setPropertyString("placeholder", "Email");
		emailPanel.add(emailTextBox);

		final HorizontalPanel codiceFiscalePanel = new HorizontalPanel();
		HTML cfLabel = new HTML("<br>Codice fiscale: "); 
		final TextBox codiceFiscaleTextBox = new TextBox();
		codiceFiscaleTextBox.getElement().setPropertyString("placeholder", "Codice Fiscale");
		codiceFiscalePanel.add(codiceFiscaleTextBox);

		final HorizontalPanel indirizzoPanel = new HorizontalPanel();
		HTML indirizzoLabel = new HTML("<br>Indirizzo: "); 
		final TextBox indirizzoTextBox = new TextBox();
		indirizzoTextBox.getElement().setPropertyString("placeholder", "Indirizzo");
		indirizzoPanel.add(indirizzoTextBox);

		final HorizontalPanel sessoPanel = new HorizontalPanel();
		RadioButton rb0 = new RadioButton("myRadioGroup", "M");
		RadioButton rb1 = new RadioButton("myRadioGroup", "F");

		sessoPanel.add(rb0);
		sessoPanel.add(rb1);

		final HorizontalPanel dataNPanel = new HorizontalPanel();
		DateBox dateBox = new DateBox();
		DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
		dateBox.setFormat(new DateBox.DefaultFormat(format));
		dateBox.getElement().setPropertyString("placeholder", "DD/MM/YYYY");
		dataNPanel.add(dateBox);

		final HorizontalPanel luogoNPanel = new HorizontalPanel();
		final TextBox luogoNTextBox = new TextBox();
		luogoNTextBox.getElement().setPropertyString("placeholder", "Luogo di nascita");
		luogoNPanel.add(luogoNTextBox);

		//creo il vertical panel che conterrà tutti i campi della registrazione
		final VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("100%");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
		HTML sessoLabel = new HTML("<br>Sesso: ");
		HTML dataNLabel = new HTML("<br>Data di Nascita: ");
		HTML luogoNLabel = new HTML("<br>Luogo di nascita: ");
		formPanel.add(usernameLabel);
		formPanel.add(usernamePanel);
		
		formPanel.add(passwordLabel);
		formPanel.add(passwordPanel);
		
		formPanel.add(nameLabel);
		formPanel.add(nomePanel);
		
		formPanel.add(cognomeLabel);
		formPanel.add(cognomePanel);
		
		formPanel.add(telefonoLabel);
		formPanel.add(telefonoPanel);
		
		formPanel.add(emailLabel);
		formPanel.add(emailPanel);
		
		formPanel.add(cfLabel);
		formPanel.add(codiceFiscalePanel);
		
		formPanel.add(indirizzoLabel);
		formPanel.add(indirizzoPanel);
		
		formPanel.add(new HTML("<br><b>Opzionali:</b>"));
		
		formPanel.add(dataNLabel);
		formPanel.add(dataNPanel);
		
		formPanel.add(sessoLabel);
		formPanel.add(sessoPanel);
		
		formPanel.add(luogoNLabel);
		formPanel.add(luogoNPanel);

		//creo il dialogbox per dare lo stile al modulo di registrazione
		DialogBox boxRegister = new DialogBox();
		boxRegister.setText("Registrati");
		boxRegister.setAnimationEnabled(true);
		boxRegister.setWidth("100%");

		//click handler per uscire dal modulo
		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				onModuleLoad();
			}
		});

		//click handler per confermare la registrazione
		final Button send = new Button("Send");
		send.addClickHandler(new ClickHandler() {
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(ClickEvent arg0) {
				String dataN="";
				String sesso="";
				if(rb0.getValue()) {
					sesso="M";
				} else if(rb1.getValue()) {
					sesso="F";
				}
				if (!(dateBox.getValue()==null)) {
					dataN=format.format(dateBox.getValue());
				} else {
					dataN="noData";
				}
				sendRegister(usernameTextBox.getText(),passwordTextBox.getText(),nomeTextBox.getText(),cognomeTextBox.getText(),
						telefonoTextBox.getText(),emailTextBox.getText(),codiceFiscaleTextBox.getText(),indirizzoTextBox.getText(),sesso,
						dataN,luogoNTextBox.getText());
			}
		});

		final HorizontalPanel buttonsRegister = new HorizontalPanel();

		buttonsRegister.add(send);
		buttonsRegister.add(esci);
		formPanel.add(buttonsRegister);
		//aggiungo il vertical panel al dialogbox
		boxRegister.setWidget(formPanel);
		//mostro il dialogbox
		RootPanel.get("boxRegister").add(boxRegister);

	}

	//metodo per mostrare il modulo di login
	private void showSchermataLogin() {
		//aggiungo i campi del metodo login
		final HorizontalPanel usernamePanel = new HorizontalPanel();
		final TextBox usernameTextBox = new TextBox();
		usernameTextBox.getElement().setPropertyString("placeholder", "Username");
		usernamePanel.add(usernameTextBox);

		final HorizontalPanel passwordPanel = new HorizontalPanel();
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		passwordTextBox.getElement().setPropertyString("placeholder", "*******");
		passwordPanel.add(passwordTextBox);

		//aggiungo i capi al vertical panel
		final VerticalPanel formPanel = new VerticalPanel();
		formPanel.add(usernamePanel);
		formPanel.add(passwordPanel);

		//creo il vertical panel per la visualizzazione
		DialogBox boxLogin = new DialogBox();
		boxLogin.setText("Login");
		boxLogin.setAnimationEnabled(true);

		//click handler per uscire dal modulo
		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				onModuleLoad();
			}

		});
		//click handler per confermare il login
		final Button send = new Button("Send");
		send.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				sendLogin(usernameTextBox.getText(),passwordTextBox.getText());
			}
		});


		final HorizontalPanel buttonsLogin = new HorizontalPanel();

		buttonsLogin.add(send);
		buttonsLogin.add(esci);
		formPanel.add(buttonsLogin);
		//aggiungo il vertical panel al dialogbox
		boxLogin.setWidget(formPanel);
		//mostro il dialogbox a schermo
		RootPanel.get("boxLogin").add(boxLogin);
	}

	//metodo che invoca il login al server
	private void sendLogin(String username, String password) {

		greetingService.login(username, password, new AsyncCallback<String>() {
			//errore se fallisce e non ricevo risposta
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			//se va a buon fine mi viene ritornata una stringa contente il nome utente della persona che ha fatto il login
			public void onSuccess(String result) {
				if(result.equals("us o pw errati")) {
					Window.alert(result);
					onModuleLoad();
				} else if(result.equals("admin")){
					showAdmin();
				} else {
					RootPanel.get().clear();
					showUtente(result);
				}
			}
				
		});
			
	}
		

	//metodo del server per completare la registrazione
	private void sendRegister(String username, String password, String nome, String cognome, String telefono, String email, String codiceFiscale, String indirizzo,
			String sesso, String dataNascita, String luogoNascita){
		greetingService.register(username, password, nome, cognome, telefono, email, codiceFiscale, indirizzo, sesso, dataNascita, luogoNascita, new AsyncCallback<String>() {
			//se fallisce restituisce il messaggio di errore da parte del server
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			//se va a buon fine mi stampa la stringa di successo corrispondente a result
			public void onSuccess(String result) {
				Window.alert(result);
				onModuleLoad();
			}
		});
	}

	//metodo che mi reindirizza alla schermata personale dell'utente che ha eseguito il login
	private void showUtente(String user) {
		SchermataUtente su= new SchermataUtente(user, greetingService, this);
		su.showSchermata();
	}
	
	private void showAdmin() {
		SchermataAdmin a = new SchermataAdmin(greetingService,this);
		a.showSchermata();
	}
	

}
