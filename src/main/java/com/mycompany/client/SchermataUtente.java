package com.mycompany.client;

import java.util.ArrayList;
import java.util.Date;

import org.eclipse.jdt.core.dom.ThisExpression;

import java.text.SimpleDateFormat;
import java.lang.Math;
import com.google.gwt.i18n.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.Format;
import com.mycompany.shared.*;
import com.mycompany.server.*;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;

public class SchermataUtente{

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	private static final String NUMBER_INPUT_ERROR = "You must insert a number. Not a String!";
	private static final String OBJECT_ERROR = "No object found.";
	private static final String PRICE_ERROR = "Not a valid price.";

	private String username;
	private GreetingServiceAsync gtasync;
	public ByeBay index;

	public SchermataUtente (String username, GreetingServiceAsync gtasync, ByeBay index) {
		this.username=username;
		this.gtasync=gtasync;
		this.index=index;
	}

	public void showSchermata() {
		
		index.clear();
		RootPanel.get().clear();
		FaiDomanda indexDomanda = new FaiDomanda(this.username, this, this.gtasync);

		ColonnaCategoria colonnaDelleCategorie = new ColonnaCategoria(username, this, gtasync);
		DaiRisposta dr=new DaiRisposta(this.username,this,this.gtasync);
		colonnaDelleCategorie.sendCercaCategorie();
		
		HorizontalPanel hpInsOgg=new HorizontalPanel();
		VerticalPanel vpwelcome=new VerticalPanel();
		vpwelcome.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);

		Button insOggetto = new Button("Inserisci oggetto");
		Button searchOggetto = new Button("Cerca un oggetto");
		Button viewQuestions = new Button("Domande ricevute");
		Button viewAnswer = new Button("Risposte date");
		Button viewObjects = new Button("I tuoi oggetti");
		Button viewOffers = new Button("Le tue offerte");
		Button logout = new Button("Logout");

		viewOffers.setEnabled(true);
		viewQuestions.setEnabled(true);
		viewAnswer.setEnabled(true);
		insOggetto.setEnabled(true);
		searchOggetto.setEnabled(true);
		logout.setEnabled(true);
		InserisciOggetto insOgg=new InserisciOggetto(username,gtasync,this);
		//click handler per la vendita di un oggetto
		insOggetto.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				insOggetto.setEnabled(false);
				viewOffers.setEnabled(false);
				viewQuestions.setEnabled(false);
				viewAnswer.setEnabled(false);
				searchOggetto.setEnabled(false);
				logout.setEnabled(false);
				insOgg.inserisciOggetto();
				viewObjects.setEnabled(false);
			}
		});

		//visualizzare le domande ricevute
		viewQuestions.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				insOggetto.setEnabled(false);
				viewOffers.setEnabled(false);
				viewQuestions.setEnabled(false);
				searchOggetto.setEnabled(false);
				viewAnswer.setEnabled(false);
				logout.setEnabled(false);
				viewObjects.setEnabled(false);
				dr.sendCercaDomande(username);
			}
		});

		//visualizzare le domande ricevute
		viewAnswer.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				insOggetto.setEnabled(false);
				viewOffers.setEnabled(false);
				viewQuestions.setEnabled(false);
				viewAnswer.setEnabled(false);
				searchOggetto.setEnabled(false);
				logout.setEnabled(false);
				viewObjects.setEnabled(false);
				sendCercaRisposte(username);
			}
		});

		//mostra offerte fatte verso altri oggetti
		viewOffers.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				insOggetto.setEnabled(false);
				viewOffers.setEnabled(false);
				viewQuestions.setEnabled(false);
				searchOggetto.setEnabled(false);
				viewAnswer.setEnabled(false);
				logout.setEnabled(false);
				viewObjects.setEnabled(false);
				sendCercaOfferte(username);
			}
		});

		//mostra gli oggetti posti in vendita
		viewObjects.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				insOggetto.setEnabled(false);
				viewOffers.setEnabled(false);
				viewQuestions.setEnabled(false);
				viewAnswer.setEnabled(false);
				searchOggetto.setEnabled(false);
				logout.setEnabled(false);
				viewObjects.setEnabled(false);
				sendCercaTuoiOggetti(username);
			}
		});

		logout.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				index.onModuleLoad();
			}

		});
		UserInfo uinfo=new UserInfo(this.username,this.gtasync);
		uinfo.sendShowUserInfo(this.username);
		HTML welcome = new HTML("<p style=\"font-size:18px; font-family:Helvetica;\"><b>Benvenuto " + this.username + "!</b></p>");
		hpInsOgg.add(insOggetto);
		hpInsOgg.add(searchOggetto);
		hpInsOgg.add(viewQuestions);
		hpInsOgg.add(viewOffers);
		hpInsOgg.add(viewObjects);
		hpInsOgg.add(viewAnswer);
		hpInsOgg.add(logout);

		vpwelcome.setWidth("100%");
		vpwelcome.add(welcome);
		vpwelcome.add(hpInsOgg);
		RootPanel.get().add(vpwelcome);


		insOggetto.setEnabled(true);
		searchOggetto.setEnabled(true);
		logout.setEnabled(true);
		viewQuestions.setEnabled(true);
		viewAnswer.setEnabled(true);
		viewOffers.setEnabled(true);
		//click per la ricerca di un ogggetto

		CercaOggetto cOInPiattaforma=new CercaOggetto(this.username,this,this.gtasync);
		searchOggetto.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				insOggetto.setEnabled(false);
				viewOffers.setEnabled(false);
				viewQuestions.setEnabled(false);
				searchOggetto.setEnabled(false);
				viewAnswer.setEnabled(false);
				logout.setEnabled(false);
				
				//istanziare una classe di CercaOggetto
				cOInPiattaforma.cercaOggetto();
			}

		});
	}


	private void sendCercaTuoiOggetti (String username) {
		gtasync.cercaTuoiOggetti(username, new AsyncCallback<ArrayList<Oggetto>>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			public void onSuccess(ArrayList<Oggetto> result) {
				if(result.isEmpty()) {
					Window.alert(OBJECT_ERROR);
					showSchermata();
				}else {
					mostraTuoiOggetti(result);
				}
			}
		});

	}
	//mi servo del metodo scritto in ByeBay per stampare tutti gli oggetti con username uguale a quello inserito
	//In particolare: tutti gli oggetti messi in vendita dall'username vengono passati come parametro al metodo
	// visualizzaInformazioniOggetto di ByeBay
	private void mostraTuoiOggetti(ArrayList<Oggetto> tuoiOggetti) {
		RootPanel.get().clear();
		index.clear();
		ObjectInfo oi = new ObjectInfo(this.username,this,gtasync);
		for (int i = 0; i<tuoiOggetti.size(); i++) {
			oi.visualizzaInformazioniOggetto(tuoiOggetti.get(i));
		}
	}

	private void sendCercaRisposte(String username) {
		gtasync.cercaRisposte(username, new AsyncCallback<ArrayList<Risposta>>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			public void onSuccess(ArrayList<Risposta> result) {
				//se l'arraylist che mi ritorna è vuoto, allora non ho trovato l'oggetto
				if(result.isEmpty()) {
					Window.alert(OBJECT_ERROR);
					showSchermata();
				}else { //passo l'array list contenente gli oggetti al metodo che li mostrerà
					viewRisposte(result);
				}
			}
		});
	}


	private void viewRisposte(ArrayList<Risposta> risposta){
		RootPanel.get().clear();
		index.clear();
		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("300");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		final VerticalPanel foundObjectsPanel = new VerticalPanel();
		for (int i = 0; i < risposta.size();i++){
				HTML risposte=new HTML("<p><br><br></p>"+
						"<br>"+"Nome oggetto: "+risposta.get(i).getInstanceDomanda().getOggettoDomanda().getNome()+"</br>"+
						"<br>"+"Mittente: "+risposta.get(i).getInstanceDomanda().getMittente()+"</br>"+
						"<br>"+"destinatario: "+risposta.get(i).getInstanceDomanda().getOggettoDomanda().getProprietario()+"</br>"+
						"<br>"+"Domanda: "+risposta.get(i).getInstanceDomanda().getQuestion()+"</br>"+
						"<br>"+"Risposta: "+risposta.get(i).getAnswer()+"</br>");

				foundObjectsPanel.add(risposte);
				formPanel.add(foundObjectsPanel);
	}

		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Domande ricevute");
		boxInsertObject.setAnimationEnabled(true);
		boxInsertObject.add(formPanel);
		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");


		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				if(username != null) {
					showSchermata();
				}else {
					Window.alert("Prima devi registrarti alla piattaforma");
				}
			}

		});

		final HorizontalPanel buttonsSellExit = new HorizontalPanel();

		buttonsSellExit.add(esci);
		boxInsertObject.setWidget(formPanel);
		formPanel.add(buttonsSellExit);
		RootPanel.get().add(boxInsertObject);
	}



	private void sendCercaOfferte(String username) {
		gtasync.cercaOfferte(username, new AsyncCallback<ArrayList<Offerta>>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			public void onSuccess(ArrayList<Offerta> result) {
				if(result.isEmpty()) {
					Window.alert(OBJECT_ERROR);
					showSchermata();
				}else {
					viewOfferte(result);
				}
			}
		});
	}


	private void viewOfferte(ArrayList<Offerta> offerta){
		RootPanel.get().clear();
		index.clear();
		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("300");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		final VerticalPanel foundObjectsPanel = new VerticalPanel();
		//HTML nomeOggetto=new HTML("Nome oggetto trovato: "+offerta.get(0).getNome());

		for (int i = 0; i < offerta.size();i++)	{

			formPanel.add(foundObjectsPanel);
			HTML domande=new HTML("Nome oggetto: "+offerta.get(i).getOggetto().getNome()+
					"<br>"+"Stato: "+offerta.get(i).getStato()+"</br>"+
					"<br>"+"importo: "+offerta.get(i).getImporto()+"</br>"+
					"<p><br><br></p>");
			foundObjectsPanel.add(domande);
			formPanel.add(foundObjectsPanel);
		}

		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Lo stato delle tue offerte");
		boxInsertObject.setAnimationEnabled(true);
		boxInsertObject.add(formPanel);
		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");

		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				if(username != null) {
					showSchermata();
				}else {
					Window.alert("Prima devi registrarti alla piattaforma");
				}
			}
		});

		final HorizontalPanel buttonsSellExit = new HorizontalPanel();

		buttonsSellExit.add(esci);
		boxInsertObject.setWidget(formPanel);
		formPanel.add(buttonsSellExit);
		RootPanel.get().add(boxInsertObject);
	}
}
