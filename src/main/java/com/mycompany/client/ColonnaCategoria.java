package com.mycompany.client;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Math;
import com.google.gwt.i18n.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.Format;
import com.mycompany.shared.*;
import com.mycompany.server.*;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
public class ColonnaCategoria {
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	private static final String NUMBER_INPUT_ERROR = "You must insert a number. Not a String!";
	private static final String OBJECT_ERROR = "No object found.";
	private static final String PRICE_ERROR = "Not a valid price.";

	private String username;
	private String nomeOggettoOfferto;
	private GreetingServiceAsync gtasync;
	private SchermataUtente indexProfilo;
	public ColonnaCategoria(String username, SchermataUtente indexProfilo, GreetingServiceAsync gtasync) {
		this.username = username;
		this.indexProfilo = indexProfilo;
		this.gtasync = gtasync;
	}
	ArrayList<SottoCategoria> nomiTutteSottoCategorie= new ArrayList<SottoCategoria>();

	public void sendAllSubCategories() {
		gtasync.cercaSottoCategoria(new AsyncCallback<ArrayList<SottoCategoria>>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<SottoCategoria> result) {
				nomiTutteSottoCategorie = result; 

			}
		});
	}
	//ArrayList contenente tutte le categorie
	ArrayList<Categoria> nomiTutteCategorie = new ArrayList<Categoria>();
	public void sendAllCategories() {
		gtasync.cercaCategoria(new AsyncCallback<ArrayList<Categoria>>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<Categoria> result) {
				nomiTutteCategorie = result; 

			}
		});
	}
	
	//metodo che stampa tutti gli oggetti appartenenti a quella categoria
	public VerticalPanel mostraOggettiCategorie(String categoria) {
		FaiDomanda indexDomanda = new FaiDomanda(this.username, indexProfilo, this.gtasync);
		ObjectInfo oi = new ObjectInfo(this.username,indexProfilo, this.gtasync);
		FaiOfferta indexOfferta = new FaiOfferta(this.username, indexProfilo, this.gtasync);
		FlexTable stocksFlexTable = new FlexTable();
		VerticalPanel mainPanel = new VerticalPanel();
		HorizontalPanel addPanel = new HorizontalPanel();
		//l'oggetto oP mi servità per richiamare i metodi fai offerta e fai domanda

		gtasync.showAllObjects(new AsyncCallback<ArrayList<Oggetto>>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<Oggetto> oggettiTrovati) {
				
				HTML titleOP = new HTML("<h3>Oggetti presenti nella piattaforma</h3>");

				//creo la tabella
				stocksFlexTable.setText(0, 0, "Nome");
				stocksFlexTable.setText(0, 1, "Proprietario");
				stocksFlexTable.setText(0, 2, "Prezzo iniziale");
				stocksFlexTable.setText(0, 3, "Data scadenza offerta");
				stocksFlexTable.setText(0, 4, "Categoria");
				stocksFlexTable.setText(0, 5, "Descrizione");

				Button tornaIndietro = new Button("Torna indietro");
				tornaIndietro.setEnabled(true);
				tornaIndietro.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent arg0) {
						RootPanel.get().clear();
						indexProfilo.showSchermata();
					}
				});
				stocksFlexTable.setWidget(0, 6, tornaIndietro);
				// Aggiungo lo stile
				stocksFlexTable.getRowFormatter().addStyleName(0, "displayOggetti");
				stocksFlexTable.addStyleName("listaOggettiHome");
				stocksFlexTable.getCellFormatter().addStyleName(0, 1, "listaNumericColumn");
				stocksFlexTable.getCellFormatter().addStyleName(0, 2, "listaNumericColumn");

				addPanel.addStyleName("aggiuntaPanel");
				
				//creo una stringa che contrene tutte le figlie della categoria passata in input sparate da un -
				String allSubCategoriesOfCatInput="";
				for(int i=0;i<nomiTutteSottoCategorie.size();i++) {
					if(nomiTutteSottoCategorie.get(i).getCategoriaMadre().matches("(.*)"+categoria+("(.*)"))) {
						allSubCategoriesOfCatInput+=nomiTutteSottoCategorie.get(i).getNome()+"-";
					}
				}
				
				for (int i = 0; i < oggettiTrovati.size();i++)	{
					//controllo se la categoria dell'oggetto è uguale alla categoria in input o metcha con una delle sue figlie
					if(oggettiTrovati.get(i).getCategoria().equals(categoria) || allSubCategoriesOfCatInput.matches("(.*)"+oggettiTrovati.get(i).getCategoria()+("(.*)"))) {
						if (oggettiTrovati.get(i).getStato().equals("Asta in corso")) {
							
							HTML nome = new HTML("<a href=#>"+oggettiTrovati.get(i).getNome()+"</a>");
							final int count=i;
							nome.getElement().getStyle().setColor("#00FF00 ");
							nome.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent event) {
									//rimuovo tutto il resto dalla schermata
									RootPanel.get().clear();
									gtasync.domandeRisposte(oggettiTrovati.get(count), new AsyncCallback<ArrayList<String>>() {
										public void onFailure(Throwable caught) {
											Window.alert(caught.getMessage());
										}
										public void onSuccess(ArrayList<String> result) {
											RootPanel.get().clear();
											oi.visualizzaInformazioniOggetto2(oggettiTrovati.get(count),result);
										}
									});
								}
							});
							stocksFlexTable.setWidget(i+1, 0, nome);

							stocksFlexTable.setText(i+1, 1, oggettiTrovati.get(i).getProprietario());
							//converto il prezzo double in stringa
							String prezzoStringa = oggettiTrovati.get(i).getPrezzo().toString();
							stocksFlexTable.setText(i+1, 2, prezzoStringa);
							stocksFlexTable.setText(i+1, 3, oggettiTrovati.get(i).getDataScadenza());
							stocksFlexTable.setText(i+1, 4, oggettiTrovati.get(i).getCategoria());
							stocksFlexTable.setText(i+1, 5, oggettiTrovati.get(i).getDescrizione());

							//aggiungo il pulsante per vedere più informazioni di quell'oggetto
							final Button faiOffertaBtn = new Button("Fai un'offerta");
							final Button faiDomanda = new Button("Fai una Domanda");

							//se all'interno del click usassi "i" darebbe errore

							//click per fare offerte e domande per quel determinato oggetto
							faiOffertaBtn.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent arg0) {
									indexOfferta.mostraSchermataOfferta(oggettiTrovati.get(count));
								}
							});
							stocksFlexTable.setWidget(i+1, 6, faiOffertaBtn);

							faiDomanda.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent arg0) {
									indexDomanda.mostraSchermataDomanda(oggettiTrovati.get(count));
								}
							});
							stocksFlexTable.setWidget(i+1, 7, faiDomanda);
						}
					}

				}
				mainPanel.add(titleOP);
				mainPanel.add(stocksFlexTable);
				mainPanel.add(addPanel);
			}
			
		});
		
		return mainPanel;

	}
	
	public void sendCercaCategorie () {
		sendAllCategories();
		sendAllSubCategories();
		//questo metodo showAllObjects mi restituirà tutti gli oggetti
		gtasync.cercaCategoria(new AsyncCallback<ArrayList<Categoria>>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			public void onSuccess(ArrayList<Categoria> result) {
				mostraCategoriePresenti(result);
			}
		});

	}
	//prendi in input il categoriaPresente.get(count).getNome() per poi stampare SOLO le sue categorie figlie
	public void sendCercaSottoCategorie (String nomeCategoriaMadre) {
		//questo metodo showAllObjects mi restituirà tutti gli oggetti
		gtasync.cercaSottoCategoria(new AsyncCallback<ArrayList<SottoCategoria>>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			public void onSuccess(ArrayList<SottoCategoria> result) {
				mostraSottoCategorie(nomeCategoriaMadre,result);

			}
		});
	}

	VerticalPanel oggettiDelleSottoCategorie = new VerticalPanel();
	HorizontalPanel containerPanel = new HorizontalPanel ();
	public void mostraSottoCategorie(String nomeCategoriaMadre, ArrayList <SottoCategoria> sottoCategoriaPresente) {

		VerticalPanel mainPanel = new VerticalPanel();
		FlexTable stocksFlexTable = new FlexTable();
		HorizontalPanel addPanel = new HorizontalPanel();

		HTML titleOP = new HTML("<h3>Sotto categorie presenti nella piattaforma</h3>");
		stocksFlexTable.setText(0, 0, "Categoria");
		stocksFlexTable.getRowFormatter().addStyleName(0, "displayOggetti");
		stocksFlexTable.addStyleName("listaOggettiHome");
		stocksFlexTable.getCellFormatter().addStyleName(0, 0, "listaNumericColumn");

		addPanel.addStyleName("aggiuntaPanel");

		// Assemble Main panel.
		mainPanel.add(titleOP);
		mainPanel.add(stocksFlexTable);
		mainPanel.add(addPanel);

		//modalità per mostrare la tabella degli oggetti affiancata alla colonna con le categorie
		//aggiungo la tabella degli oggetti proveniente da ObjectInfo
		ObjectInfo oi = new ObjectInfo(this.username,indexProfilo,gtasync);
		//containerPanel.add(oi.visualizzaTuttiGliOggetti());
		//setto lo spacing
		mainPanel.getElement().getStyle().setProperty("marginLeft", "50px");
		//aggiungo all horizontal panel la tabella delle categorie
		containerPanel.add(mainPanel);
		containerPanel.getElement().getStyle().setProperty("marginLeft", "auto");
		containerPanel.getElement().getStyle().setProperty("marginRight", "0");


		for(int i = 0; i<sottoCategoriaPresente.size(); i++) {
			//Stampo solo le categorie con la categoria madre del parametro ===> nomeCategoriaMadre

			//array  categoria madre contente solo i nomi senza "-"
			String [] sottoCatMadri = sottoCategoriaPresente.get(i).getCategoriaMadre().split("-");
			if(sottoCatMadri[sottoCatMadri.length-1].matches("(.*)"+nomeCategoriaMadre+"(.*)")) {

				//stocksFlexTable.setText(i+1, 0, sottoCategoriaPresente.get(i).getNome());
				Button viewObjectButton = new Button(sottoCategoriaPresente.get(i).getNome());
				viewObjectButton.setEnabled(true);

				final int count = i;
				viewObjectButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						
						//gli passo il nome madre della sottocategoria cioè: "Casa-"
						oggettiDelleSottoCategorie.clear();
						mainPanel.clear();
						oggettiDelleSottoCategorie = mostraOggettiCategorie(sottoCategoriaPresente.get(count).getNome());
						containerPanel.clear();
						
						// stampo la tabella delle sotto categorie una volta premuto sul pulsante mostra oggetti della categoria
						containerPanel.add(oggettiDelleSottoCategorie);
						containerPanel.add(mainPanel);

						sendCercaSottoCategorie(sottoCategoriaPresente.get(count).getNome());
					}
				});
				stocksFlexTable.setWidget(i+1, 0, viewObjectButton);
			}

		}

		RootPanel.get().add(containerPanel);
	}
	//vertical panel degli oggetti per di mostraOggettiCategorie
	//questo vertical panel verrà utilizzato una volta cliccato sul pulsante mostra oggetti categorie
	VerticalPanel oggettiDelleCategorie = new VerticalPanel();
	//mostra le varie categorie presenti
	public void mostraCategoriePresenti(ArrayList <Categoria> categoriaPresente) {
		VerticalPanel mainPanel = new VerticalPanel();
		FlexTable stocksFlexTable = new FlexTable();
		HorizontalPanel addPanel = new HorizontalPanel();

		HTML titleOP = new HTML("<h3>Categorie presenti nella piattaforma</h3>");
		stocksFlexTable.setText(0, 0, "Categoria");
		stocksFlexTable.getRowFormatter().addStyleName(0, "displayOggetti");
		stocksFlexTable.addStyleName("listaOggettiHome");
		stocksFlexTable.getCellFormatter().addStyleName(0, 0, "listaNumericColumn");

		addPanel.addStyleName("aggiuntaPanel");

		// Assemble Main panel.
		mainPanel.add(titleOP);
		mainPanel.add(stocksFlexTable);
		mainPanel.add(addPanel);

		//aggiungo la tabella degli oggetti proveniente da ObjectInfo
		ObjectInfo oi = new ObjectInfo(this.username,indexProfilo,gtasync);
		containerPanel.add(oi.visualizzaTuttiGliOggetti());
		//setto lo spacing
		mainPanel.getElement().getStyle().setProperty("marginLeft", "50px");
		//aggiungo all horizontal panel la tabella delle categorie
		containerPanel.add(mainPanel);
		containerPanel.getElement().getStyle().setProperty("marginLeft", "auto");
		containerPanel.getElement().getStyle().setProperty("marginRight", "0");

		for(int i = 0; i<categoriaPresente.size(); i++) {
			//stocksFlexTable.setText(i+1, 0, categoriaPresente.get(i).getNome());
			Button viewObjectButton = new Button(categoriaPresente.get(i).getNome());
			viewObjectButton.setEnabled(true);

			final int count = i;
			viewObjectButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					oggettiDelleCategorie = mostraOggettiCategorie(categoriaPresente.get(count).getNome());
					containerPanel.clear();
					mainPanel.clear();
					// stampo la tabella delle categorie una volta premuto sul pulsante mostra oggetti della categoria
					containerPanel.add(oggettiDelleCategorie);
					containerPanel.add(mainPanel);
					sendCercaSottoCategorie(categoriaPresente.get(count).getNome());
				}
			});
			stocksFlexTable.setWidget(i+1, 0, viewObjectButton);
		}

		RootPanel.get().add(containerPanel);
	}
}


