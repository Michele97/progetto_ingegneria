package com.mycompany.client;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Math;
import com.google.gwt.i18n.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.Format;
import com.mycompany.shared.*;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;


public class CercaOggetto {

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	private static final String NUMBER_INPUT_ERROR = "You must insert a number. Not a String!";
	private static final String OBJECT_ERROR = "No object found.";
	private static final String PRICE_ERROR = "Not a valid price.";
	//settare l'username a sconosciuto
	private String username;
	private String nomeOggettoOfferto;
	private SchermataUtente indexProfilo;
	private ByeBay indexHome;
	private ObjectInfo indexObjectInfo;
	private GreetingServiceAsync gtasync;
	//servirà per richiamare questa classe anche dalla classe HOME MYMODULE
	//private ByeBay index;
	private String offerenteMaggiore = "";

	public CercaOggetto(String username, SchermataUtente index, GreetingServiceAsync gtasync) {
		this.username = username;
		this.indexProfilo = index;
		this.gtasync = gtasync;
	}
	
	
	//costruttore per richiamare il metodo nella classe ByeBay (che sarebbe la pagina pricipale)
	public CercaOggetto(String username, ByeBay indexHome, GreetingServiceAsync gtasync) {
		this.username = username;
		this.indexHome = indexHome;
		this.gtasync = gtasync;
	}
		

	int counter = 0;
	public void mostraOggettiTrovati(ArrayList<Oggetto> oggettiTrovati) {
		FaiDomanda indexDomanda = new FaiDomanda(this.username, indexProfilo, this.gtasync);
		FaiOfferta indexOfferta = new FaiOfferta(this.username,indexProfilo,this.gtasync);
		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("100%");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		final VerticalPanel foundObjectsPanel = new VerticalPanel();
		int i = 0;
		for (i = 0; i < oggettiTrovati.size();i++)	{ // al momento stampo solo il nome degli oggetti
			
			//controllo che l'utente sia effettivamente entrato
			if (this.username!="sconosciuto") {
				//aggiungo il pulsante per fare un offerta
				final Button faiOffertaBtn = new Button("Fai un'offerta");
				final int counter = i;
				faiOffertaBtn.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent arg0) {
						indexOfferta.mostraSchermataOfferta(oggettiTrovati.get(counter));

					}
				});
				final Button faiDomanda = new Button("Fai una Domanda");

				faiDomanda.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent arg0) {
						indexDomanda.mostraSchermataDomanda(oggettiTrovati.get(counter));

					}
				});

				foundObjectsPanel.add(faiDomanda);

				//per semplicizzare e non passargli sempre counter lo aggiorno
				foundObjectsPanel.add(faiOffertaBtn);
			}		
			formPanel.add(foundObjectsPanel);
			HTML nomeOggetto=new HTML("Nome oggetto: "+oggettiTrovati.get(i).getNome()+"<br>Proprietario: "+oggettiTrovati.get(i).getProprietario()+"<br>Categoria: "+
					oggettiTrovati.get(i).getCategoria()+"<br>Prezzo: "+oggettiTrovati.get(i).getPrezzo()+"<br>Descrizione: "+oggettiTrovati.get(i).getDescrizione()+"<br>Data di pubblicazione: "+oggettiTrovati.get(i).getDataPubblicazione()+
					"<br>Data scadenza dell'asta: "+oggettiTrovati.get(i).getDataScadenza()+"<br><br>");
			foundObjectsPanel.add(nomeOggetto);
			formPanel.add(foundObjectsPanel); //lo aggiungo al form panel ad ogni iterazione
		}
	
		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Oggetti trovati");
		boxInsertObject.setAnimationEnabled(true);
		boxInsertObject.add(formPanel);
		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");
		RootPanel.get().add(boxInsertObject);

		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				if(username == "sconosciuto") {
					indexHome.onModuleLoad();
				}else {
					indexProfilo.showSchermata();
				}
			}

		});

		final HorizontalPanel buttonsSellExit = new HorizontalPanel();

		buttonsSellExit.add(esci);
		boxInsertObject.setWidget(formPanel);
		formPanel.add(buttonsSellExit);
		RootPanel.get().add(boxInsertObject);

	}



	public void cercaOggetto() {
		if(username!="sconosciuto") {
			RootPanel.get().clear();
		} else {
			indexHome.clear();
			RootPanel.get().clear();
		}
		
		HTML primoParagrafo = new HTML("<p>Cerca un oggetto: </p><br>");

		final HorizontalPanel searchObjectPanel = new HorizontalPanel();
		final TextBox searchObjectTextBox = new TextBox();
		HTML ricercaParagrafo = new HTML("<div style=\"font-size:16px;\">Cerca un oggetto: </div>");
		searchObjectPanel.add(ricercaParagrafo);
		searchObjectPanel.add(searchObjectTextBox);


		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("300");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		formPanel.add(searchObjectPanel);

		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Cerca un oggetto");
		boxInsertObject.setAnimationEnabled(true);
		boxInsertObject.add(formPanel);
		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");

		final HorizontalPanel buttonsSellExit = new HorizontalPanel();
		final Button cerca = new Button("Cerca");
		final Button esci = new Button("Esci");

		buttonsSellExit.add(esci);
		buttonsSellExit.add(cerca);
		formPanel.add(buttonsSellExit);

		boxInsertObject.setWidget(formPanel);

		RootPanel.get().add(boxInsertObject);
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				if(username!="sconosciuto") {
					indexProfilo.showSchermata();
				} else {
					indexHome.onModuleLoad();
				}
			}

		});
		cerca.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				sendCercaOggetto(searchObjectTextBox.getText());

			}
		});

	}
	
	
	private void sendCercaOggetto(String nomeOggetto) {
		gtasync.cercaOggetto(nomeOggetto, new AsyncCallback<ArrayList<Oggetto>>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			public void onSuccess(ArrayList<Oggetto> result) {
				//se l'arraylist che mi ritorna è vuoto, allora non ho trovato l'oggetto
				if(result.isEmpty()) {
					Window.alert(OBJECT_ERROR);
				}else { //passo l'array list contenente gli oggetti al metodo che li mostrerà
					mostraOggettiTrovati(result);
				}

			}
		});
	}


}
