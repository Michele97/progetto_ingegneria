package com.mycompany.client;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.ibm.icu.text.SimpleDateFormat;
import com.mycompany.shared.*;

public class InserisciOggetto {

	private String username;
	private GreetingServiceAsync gtasync;
	private SchermataUtente su;
	private String categoriaScelta;
	public InserisciOggetto(String username,GreetingServiceAsync gtasync, SchermataUtente su) {
		this.username=username;
		this.gtasync=gtasync;
		this.su=su;
	}

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";


	public void inserisciOggetto() {
		RootPanel.get().clear();
		su.index.clear();
		String name= this.username;
		DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");

		final HorizontalPanel nameObjectPanel = new HorizontalPanel();
		final TextBox objectTextBox = new TextBox();
		HTML textName = new HTML("<div style=\"font-size:16px;\">Nome oggetto: </div>");
		objectTextBox.getElement().setPropertyString("placeholder", "Inserisci il nome dell'oggetto");
		nameObjectPanel.add(textName);
		nameObjectPanel.add(objectTextBox);

		final HorizontalPanel ownerPanel = new HorizontalPanel();
		HTML user=new HTML("Nome utente: "+this.username);
		ownerPanel.add(user);

		final HorizontalPanel categoryPanel = new HorizontalPanel();
		HTML textCategory = new HTML("<div style=\"font-size:16px;\">Categoria: </div>");
		ListBox lb = new ListBox();
		
		//metodo che cerca tutte le categorie che poi verranno stampate nella listbox
		//devo cercare le categorie ma anche le sottocategorie
		gtasync.cercaCategoria(new AsyncCallback<ArrayList<Categoria>>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<Categoria> result) {
				for(int i = 0; i<result.size(); i++) {
					lb.addItem(result.get(i).getNome());
				}
			}
		});
	
		categoryPanel.add(textCategory);
		gtasync.cercaSottoCategoria(new AsyncCallback<ArrayList<SottoCategoria>>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<SottoCategoria> result) {
				for(int i = 0; i<result.size(); i++) {
					lb.addItem(result.get(i).getNome());
				}
			}
		});
		categoryPanel.add(lb);
		lb.setVisibleItemCount(1);

		final HorizontalPanel pricePanel = new HorizontalPanel();
		final TextBox priceTextBox = new TextBox();
		HTML textPrice = new HTML("<div style=\"font-size:16px;\">Prezzo: </div>");
		priceTextBox.getElement().setPropertyString("placeholder", "Inserisci il prezzo");
		pricePanel.add(textPrice);
		pricePanel.add(priceTextBox);


		final HorizontalPanel datePanel = new HorizontalPanel();
		final DateBox dateTextBox = new DateBox();
		dateTextBox.setFormat(new DateBox.DefaultFormat(format));
		dateTextBox.getElement().setPropertyString("placeholder", "DD/MM/YYYY HH:MM");
		HTML strScadenza = new HTML("<div style=\"font-size:16px;\">Scadenza: </div>");
		datePanel.add(strScadenza);
		datePanel.add(dateTextBox);

		final HorizontalPanel descriptHtml = new HorizontalPanel();
		HTML descriptionObHtml = new HTML("Descrizione dell'oggetto: ");
		final HorizontalPanel descriptionPanel = new HorizontalPanel();
		final TextArea descriptionTextArea = new TextArea();
		descriptionTextArea.setCharacterWidth(55);
		descriptionTextArea.setVisibleLines(5);
		descriptHtml.add(descriptionObHtml);
		descriptionPanel.add(descriptionTextArea);


		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("300");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		formPanel.add(ownerPanel);
		formPanel.add(categoryPanel);
		formPanel.add(nameObjectPanel);
		formPanel.add(pricePanel);
		formPanel.add(datePanel);
		formPanel.add(descriptHtml);
		formPanel.add(descriptionPanel);

		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Inserisci l'oggetto che hai intenzione di vendere");
		boxInsertObject.setAnimationEnabled(true);
		boxInsertObject.add(formPanel);
		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");


		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				RootPanel.get().clear();
				su.showSchermata();
			}

		});

		final Button vendi = new Button("Vendi");
		vendi.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				categoriaScelta = lb.getSelectedItemText();
				String dataI = format.format( new Date() ).toString();
				String dataS = format.format(dateTextBox.getValue());
				//controllo se la data è maggiore di quella odierna
				//in particolare: la data di scadenza deve essere maggiore di quella odierna
				//Non possso inserire un oggetto con data passata
				DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");

				Date dataScadenzaOggetto;
				Date dateInserimentoOggetto;

				dataScadenzaOggetto = sdf.parse(dataS);
				dateInserimentoOggetto = sdf.parse(dataI);


				if(!dataScadenzaOggetto.after(dateInserimentoOggetto)){ // se non è vero che la data di scadenza viene dopo quella di oggi ...
					Window.alert("La data di scadenza deve essere maggiore della data odierna");
					return; //forzo l'uscita in modo tale che non esegua il codice sottostante
				}
				Double p=0.0;
				if(!(priceTextBox.getValue()=="")){
					try {
						p = Double.parseDouble(priceTextBox.getText());
						p = Math.abs(p);
						 // listBox1.GetItemText(listBox1.SelectedValue) =====> è la categoria scelta
						sendVendiOggetto(name,categoriaScelta,objectTextBox.getText(), dataI, dataS, p,descriptionTextArea.getText());
						//System.out.println(name+categoryTextBox.getText()+objectTextBox.getText()+ dataI+ dataS+ p+descriptionTextArea.getText() );
					} catch(NumberFormatException e) {
						Window.alert("Il prezzo deve essere nella forma: 123.123");
					}
				}
				else {
					Window.alert("riempire il campo prezzo");
				}
			}
		});
		

		final HorizontalPanel buttonsSellExit = new HorizontalPanel();

		buttonsSellExit.add(vendi);
		buttonsSellExit.add(esci);
		formPanel.add(buttonsSellExit);

		boxInsertObject.setWidget(formPanel);

		RootPanel.get().add(boxInsertObject);

	}

	private void sendVendiOggetto(String proprietario, String categoria, String nome, String dataPubblicazione, String dataScadenza, Double prezzo, String descrizione){

		gtasync.vendiOggetto(proprietario,  categoria,  nome,  dataPubblicazione,  dataScadenza,  prezzo,  descrizione, new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(String result) {
				Window.alert(result);
				su.showSchermata();
			}
		});

	}
}
