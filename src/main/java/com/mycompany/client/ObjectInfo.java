package com.mycompany.client;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mycompany.shared.*;

public class ObjectInfo {

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	private static final String NUMBER_INPUT_ERROR = "You must insert a number. Not a String!";
	private static final String OBJECT_ERROR = "No object found.";
	private static final String PRICE_ERROR = "Not a valid price.";

	private String username;
	private GreetingServiceAsync gtasync;
	private ByeBay index;
	private SchermataUtente indexProfilo;
	public ObjectInfo(String username, SchermataUtente indexProfilo, GreetingServiceAsync gtasync) {
		this.username = username;
		this.indexProfilo = indexProfilo;
		this.gtasync = gtasync;
	}


	//costruttore per richiamare il metodo nella classe ByeBay (che sarebbe la pagina pricipale)
	public ObjectInfo(String username, ByeBay index, GreetingServiceAsync gtasync) {
		this.username = username;
		this.index = index;
		this.gtasync = gtasync;
	}

	public VerticalPanel visualizzaTuttiGliOggetti() {
		VerticalPanel mainPanel = new VerticalPanel();
		FlexTable stocksFlexTable = new FlexTable();
		HorizontalPanel addPanel = new HorizontalPanel();
		TextBox newSymbolTextBox = new TextBox();
		final FaiOfferta indexOfferta = new FaiOfferta(username, indexProfilo, gtasync);
		FaiDomanda indexDomanda = new FaiDomanda(this.username, this.indexProfilo, this.gtasync);

		gtasync.showAllObjects(new AsyncCallback<ArrayList<Oggetto>>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<Oggetto> oggettiTrovati) {

				HTML titleOP = new HTML("<h3>Oggetti presenti nella piattaforma</h3>");

				//creo la tabella
				stocksFlexTable.setText(0, 0, "Nome");
				stocksFlexTable.setText(0, 1, "Proprietario");
				stocksFlexTable.setText(0, 2, "Prezzo iniziale");
				stocksFlexTable.setText(0, 3, "Data scadenza offerta");
				stocksFlexTable.setText(0, 4, "Categoria");
				stocksFlexTable.setText(0, 5, "Offerta attuale maggiore");

				// Aggiungo lo stile
				stocksFlexTable.getRowFormatter().addStyleName(0, "displayOggetti");
				stocksFlexTable.addStyleName("listaOggettiHome");
				stocksFlexTable.getCellFormatter().addStyleName(0, 1, "listaNumericColumn");
				stocksFlexTable.getCellFormatter().addStyleName(0, 2, "listaNumericColumn");

				addPanel.addStyleName("aggiuntaPanel");

				// Assemble Main panel.
				mainPanel.add(titleOP);
				mainPanel.add(stocksFlexTable);
				mainPanel.add(addPanel);
				mainPanel.getElement().getStyle().setProperty("marginLeft", "auto");
				mainPanel.getElement().getStyle().setProperty("marginRight", "auto");

				//Con questo ciclo inserisco nell oggetto HTML tutti gli oggetti e i loro attributi
				for (int i = 0; i < oggettiTrovati.size();i++)	{

					//voglio solo gli oggetti con lo stato APERTO
					if (oggettiTrovati.get(i).getStato().equals("Asta in corso")) {
						//aggiungo gli oggetti
						//i+1 perchè cosi salto la prima riga che è dedicata all'intestazione
						//e quindi non la stostituisco con i valori dell'oggetto
						stocksFlexTable.setText(i+1, 0, oggettiTrovati.get(i).getNome());
						stocksFlexTable.setText(i+1, 1, oggettiTrovati.get(i).getProprietario());
						//converto il prezzo double in stringa
						String prezzoStringa = oggettiTrovati.get(i).getPrezzo().toString();
						stocksFlexTable.setText(i+1, 2, prezzoStringa);
						stocksFlexTable.setText(i+1, 3, oggettiTrovati.get(i).getDataScadenza());
						stocksFlexTable.setText(i+1, 4, oggettiTrovati.get(i).getCategoria());

						final int countForMaxCalculator = i; //contatore creato perchè l'indice da ora in poi dovrà essere final
						//per trovare il prezzo maggiore, mi servo del metodo maxPrezzo situato nel server
						gtasync.maxPrezzo(oggettiTrovati.get(i).getId(), new AsyncCallback<Double>() {
							public void onFailure(Throwable caught) {
								Window.alert(SERVER_ERROR);
							}
							public void onSuccess(Double result) {
								final double prezzoMax = result;
								//se mi restituisce un valore maggiore allora è ok, se no il prezzo di partenza è
								//il valore maggiore
								if(result < oggettiTrovati.get(countForMaxCalculator).getPrezzo()) {
									String prezzoMaggioreStringa = String.valueOf(oggettiTrovati.get(countForMaxCalculator).getPrezzo());
									stocksFlexTable.setText(countForMaxCalculator+1, 5, prezzoMaggioreStringa);
								}else {
									//converto il valore double prezzoMax in stringa
									String prezzoMaggioreStringa = String.valueOf(prezzoMax);
									//lo aggiungo alla tabella nella 5 colonna(sarebbe la sesta, perchè si parte da 0)
									stocksFlexTable.setText(countForMaxCalculator+1, 5, prezzoMaggioreStringa);
								}
							}
						});

						HTML nome = new HTML("<a href=#>"+oggettiTrovati.get(i).getNome()+"</a>");
						nome.getElement().getStyle().setColor("#00FF00 ");
						final int firstCount = i;
						nome.addClickHandler(new ClickHandler() {

						    @Override
						    public void onClick(ClickEvent event) {
						    	//rimuovo tutto il resto dalla schermata
								RootPanel.get().clear();
								//viewObjectButton.setEnabled(false);
								//@Override
								gtasync.domandeRisposte(oggettiTrovati.get(firstCount), new AsyncCallback<ArrayList<String>>() {
									public void onFailure(Throwable caught) {
										Window.alert(caught.getMessage());
									}
									public void onSuccess(ArrayList<String> result) {
										RootPanel.get().clear();
										visualizzaInformazioniOggetto2(oggettiTrovati.get(firstCount),result);
									}
								});
						        //oi.visualizzaInformazioniOggetto(oggettiTrovati.get(firstCount));
						    }
						});
						stocksFlexTable.setWidget(i+1, 0, nome);


						final int count = i; // altro contatore FINAL creato, per non confonderci con quello usato prima

						if (!username.equals("sconosciuto")) {
							final Button faiOffertaBtn = new Button("Fai un'offerta");
							final Button faiDomanda = new Button("Fai una Domanda");
							//click per fare offerte e domande per quel determinato oggetto
							faiOffertaBtn.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent arg0) {
									indexOfferta.mostraSchermataOfferta(oggettiTrovati.get(count));

								}
							});
							stocksFlexTable.setWidget(i+1, 6, faiOffertaBtn);

							faiDomanda.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent arg0) {

									indexDomanda.mostraSchermataDomanda(oggettiTrovati.get(count));

								}
							});
							stocksFlexTable.setWidget(i+1, 7, faiDomanda);
						}
					}
				}
			}
		});
		return mainPanel;
	}

	public void visualizzaInformazioniOggetto(Oggetto oggettoSelezionato) {
		final HorizontalPanel nameObjectPanel = new HorizontalPanel();
		HTML nameOfObject=new HTML("Nome oggetto: "+oggettoSelezionato.getNome());
		nameObjectPanel.add(nameOfObject);

		final HorizontalPanel actualPricePanel = new HorizontalPanel();
		HTML actualPrice=new HTML("Prezzo di partenza: "+oggettoSelezionato.getPrezzo());
		actualPricePanel.add(actualPrice);

		final HorizontalPanel ownerObjectPanel = new HorizontalPanel();
		HTML ownerOfObject=new HTML("Nome proprietario: "+oggettoSelezionato.getProprietario());
		ownerObjectPanel.add(ownerOfObject);

		final HorizontalPanel categoryObjectPanel = new HorizontalPanel();
		HTML categoryOfObject=new HTML("Categoria oggetto: "+oggettoSelezionato.getCategoria());
		categoryObjectPanel.add(categoryOfObject);

		final HorizontalPanel descriptionObjectPanel = new HorizontalPanel();
		HTML descriptionOfObject=new HTML("Descrizione oggetto: "+oggettoSelezionato.getDescrizione());
		descriptionObjectPanel.add(descriptionOfObject);

		final HorizontalPanel datePubbObjectPanel = new HorizontalPanel();
		HTML datePubbOfObject=new HTML("Data di pubblicazione: "+oggettoSelezionato.getDataPubblicazione());
		datePubbObjectPanel.add(datePubbOfObject);

		final HorizontalPanel dateScadObjectPanel = new HorizontalPanel();
		HTML dateScadOfObject=new HTML("Data di scadenza: "+oggettoSelezionato.getDataScadenza());
		dateScadObjectPanel.add(dateScadOfObject);

		final HorizontalPanel stateObjectPanel = new HorizontalPanel();
		HTML stateOfObject=new HTML("Stato attuale dell'oggetto: "+oggettoSelezionato.getStato());
		stateObjectPanel.add(stateOfObject);

		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("100%");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		formPanel.add(nameObjectPanel);
		formPanel.add(actualPricePanel);

		formPanel.add(ownerObjectPanel);
		formPanel.add(categoryObjectPanel);
		formPanel.add(descriptionObjectPanel);
		formPanel.add(datePubbObjectPanel);
		formPanel.add(dateScadObjectPanel);
		formPanel.add(stateObjectPanel);

		final HorizontalPanel buttonsTurnBack = new HorizontalPanel();
		Button tornaIndietro = new Button("Torna indietro");
		tornaIndietro.setEnabled(true);
		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Oggetto");
		boxInsertObject.setAnimationEnabled(true);

		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");

		buttonsTurnBack.add(tornaIndietro);
		formPanel.add(buttonsTurnBack);
		boxInsertObject.setWidget(formPanel);
		RootPanel.get().add(boxInsertObject);
		tornaIndietro.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				//torno alla home page ByeBay load solamente se l'username è settato a sconosciuto
				//questo metodo è richiamato da SchermataUtente, e quindi se l'user è settato allora tornerò
				//alla pagina pricipale di schermata utente
				if(username.equals("sconosciuto")) {
					index.clear();
					index.onModuleLoad();
				} else if (!username.equals("sconosciuto")){
					RootPanel.get().clear();
					indexProfilo.showSchermata();
				}
			}
		});
	}
	Double prezzoMax;
	public void visualizzaInformazioniOggetto2(Oggetto oggettoSelezionato, ArrayList<String> dr) {
		final HorizontalPanel nameObjectPanel = new HorizontalPanel();
		HTML nameOfObject=new HTML("Nome oggetto: "+oggettoSelezionato.getNome());
		nameObjectPanel.add(nameOfObject);

		final HorizontalPanel actualPricePanel = new HorizontalPanel();
		HTML actualPrice=new HTML("Prezzo di partenza: "+oggettoSelezionato.getPrezzo());
		actualPricePanel.add(actualPrice);

		final HorizontalPanel maxOfferedPricePanel = new HorizontalPanel();
		//per trovare il prezzo maggiore, mi servo del metodo maxPrezzo situato nel server
		gtasync.maxPrezzo(oggettoSelezionato.getId(), new AsyncCallback<Double>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(Double result) {
				 prezzoMax = result;
				//se mi restituisce un valore maggiore allora è ok, se no il prezzo di partenza è
				//il valore maggiore
				if(result < oggettoSelezionato.getPrezzo()) {
					String prezzoMaggioreStringa = String.valueOf(oggettoSelezionato.getPrezzo());
					HTML maxOfferedPrice=new HTML("Prezzo offerto attuale: "+prezzoMaggioreStringa);
					maxOfferedPricePanel.add(maxOfferedPrice);

				}else {
					//converto il valore double prezzoMax in stringa
					String prezzoMaggioreStringa = String.valueOf(prezzoMax);
					HTML maxOfferedPrice=new HTML("Prezzo offerto attuale: "+prezzoMaggioreStringa);
					maxOfferedPricePanel.add(maxOfferedPrice);
				}
			}
		});
		//pannello per l'offerente maggiore
		final HorizontalPanel maxOfferentPanel = new HorizontalPanel();
		gtasync.allOffert( new AsyncCallback<ArrayList<Offerta>>() {
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<Offerta> result) {
				//scorro tutte le offerte presenti alla ricerca di quella che corrisponde all'oggetto preso
				//in considerazione e con quel prezzo massimo offerto
				for(int i = 0; i<result.size(); i++) {
					if((oggettoSelezionato.equals(result.get(i).getOggetto())) && (prezzoMax == result.get(i).getImporto())) {
						HTML maxOfferent=new HTML("Offerente: "+result.get(i).getOfferente());
						maxOfferentPanel.add(maxOfferent);
					}
				}
			}
		});



		final HorizontalPanel ownerObjectPanel = new HorizontalPanel();
		HTML ownerOfObject=new HTML("Nome proprietario: "+oggettoSelezionato.getProprietario());
		ownerObjectPanel.add(ownerOfObject);

		final HorizontalPanel categoryObjectPanel = new HorizontalPanel();
		HTML categoryOfObject=new HTML("Categoria oggetto: "+oggettoSelezionato.getCategoria());
		categoryObjectPanel.add(categoryOfObject);

		final HorizontalPanel descriptionObjectPanel = new HorizontalPanel();
		HTML descriptionOfObject=new HTML("Descrizione oggetto: "+oggettoSelezionato.getDescrizione());
		descriptionObjectPanel.add(descriptionOfObject);

		final HorizontalPanel datePubbObjectPanel = new HorizontalPanel();
		HTML datePubbOfObject=new HTML("Data di pubblicazione: "+oggettoSelezionato.getDataPubblicazione());
		datePubbObjectPanel.add(datePubbOfObject);

		final HorizontalPanel dateScadObjectPanel = new HorizontalPanel();
		HTML dateScadOfObject=new HTML("Data di scadenza: "+oggettoSelezionato.getDataScadenza());
		dateScadObjectPanel.add(dateScadOfObject);

		final HorizontalPanel stateObjectPanel = new HorizontalPanel();
		HTML stateOfObject=new HTML("Stato attuale dell'oggetto: "+oggettoSelezionato.getStato());
		stateObjectPanel.add(stateOfObject);

		VerticalPanel formPanel = new VerticalPanel();
		formPanel.setHeight("100");
		formPanel.setWidth("100%");
		formPanel.setSpacing(10);
		formPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		formPanel.add(nameObjectPanel);
		formPanel.add(actualPricePanel);
		formPanel.add(maxOfferedPricePanel);
		formPanel.add(maxOfferentPanel);


		formPanel.add(ownerObjectPanel);
		formPanel.add(categoryObjectPanel);
		formPanel.add(descriptionObjectPanel);
		formPanel.add(datePubbObjectPanel);
		formPanel.add(dateScadObjectPanel);
		formPanel.add(stateObjectPanel);

		for(int i=0; i<dr.size(); i++){
			final HorizontalPanel qaPanel = new HorizontalPanel();
			HTML qa=new HTML(dr.get(i));
			qaPanel.add(qa);
			formPanel.add(qaPanel);
		}

		final HorizontalPanel buttonsTurnBack = new HorizontalPanel();
		Button tornaIndietro = new Button("Torna indietro");
		tornaIndietro.setEnabled(true);
		final HorizontalPanel brpanel = new HorizontalPanel();
		HTML br = new HTML("<br><br><br>");
		brpanel.add(br);
		RootPanel.get().add(brpanel);
		DialogBox boxInsertObject = new DialogBox();
		boxInsertObject.setText("Oggetto");
		boxInsertObject.setAnimationEnabled(true);

		boxInsertObject.setWidth("100%");
		boxInsertObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertObject.getElement().getStyle().setProperty("marginRight", "auto");

		buttonsTurnBack.add(tornaIndietro);
		formPanel.add(buttonsTurnBack);
		boxInsertObject.setWidget(formPanel);
		RootPanel.get().add(boxInsertObject);
		tornaIndietro.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				if(username.equals("sconosciuto")) {
					index.clear();
					index.onModuleLoad();
				} else if (!username.equals("sconosciuto")){
					RootPanel.get().clear();
					indexProfilo.showSchermata();
				}
			}
		});
	}
}
