package com.mycompany.client;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mycompany.shared.*;

public class SchermataAdmin {
	
	private ByeBay index;
	private GreetingServiceAsync gtasync;
	
	public SchermataAdmin(GreetingServiceAsync gtasync,ByeBay index) {
		this.index=index;
		this.gtasync=gtasync;	
	}
	
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	ArrayList<Categoria> categories = new ArrayList<Categoria>();
	ArrayList<SottoCategoria> subcategories = new ArrayList<SottoCategoria>();
	ArrayList<Offerta> offerts = new ArrayList<Offerta>();
	ArrayList<Oggetto> objects = new ArrayList<Oggetto>();
	ArrayList<Risposta> answers = new ArrayList<Risposta>();
	ArrayList<Domanda> questions = new ArrayList<Domanda>();
	
	
/*******************************************************************************************************/
/**********************************SCHERMATA PRINCIPALE DELL'ADMIN**************************************/
/*******************************************************************************************************/
	
	
	public void showSchermata() {
		index.clear();
		RootPanel.get().clear();
		getAllOfferts();
		getAllCategories();
		getAllSubCategories();
		getAllObjects();
		getAllAnswers();
		getAllQuestions();
		Button insCategoria = new Button("Inserisci categoria");
		Button insSottoCategoria = new Button("Inserisci sottocategoria");
		Button renameCategoria = new Button("Rinomina categoria/sottocategoria");
		Button deleteOggetto = new Button("Elimina un oggetto");
		Button deleteOfferta = new Button("Elimina un'offerta");
		Button deleteDomanda = new Button("Elimina una domanda");
		Button deleteRisposta = new Button("Elimina una risposta");
		Button logout = new Button("Logout");
		
		HorizontalPanel buttons = new HorizontalPanel();
		buttons.getElement().getStyle().setProperty("marginLeft", "auto");
		buttons.getElement().getStyle().setProperty("marginRight", "auto");
		
		insCategoria.addStyleName("sendButton");
		insSottoCategoria.addStyleName("sendButton");
		renameCategoria.addStyleName("sendButton");
		deleteOggetto.addStyleName("sendButton");
		deleteOfferta.addStyleName("sendButton");
		deleteDomanda.addStyleName("sendButton");
		deleteRisposta.addStyleName("sendButton");
		logout.addStyleName("sendButton");
		
		buttons.add(insCategoria);
		buttons.add(insSottoCategoria);
		buttons.add(renameCategoria);
		buttons.add(deleteOggetto);
		buttons.add(deleteOfferta);
		buttons.add(deleteDomanda);
		buttons.add(deleteRisposta);
		buttons.add(logout);
		
		RootPanel.get().add(buttons);
		
		insCategoria.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showInsCategory();
			}
		});
		
		insSottoCategoria.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showInsSubCategory();
			}
		});
		
		deleteOfferta.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showDeleteOfferta();
			}
		});
		
		renameCategoria.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showRenameCategory();
			}
		});
		
		deleteOggetto.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showRemoveObject();
			}
		});
		
		deleteDomanda.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showRemoveQuestion();
			}
		});
		
		deleteRisposta.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showRemoveAnswers();
			}
		});
		
		logout.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				index.onModuleLoad();
			}
		});
	}
	
	
/*******************************************************************************************************/
/*********METODI SHOW PER VISUALIZZARE I PANNELLI PER INSERIRE I DATI DA MANDARE AL SERVER**************/
/************************E PER VISUALIZZARE LA LISTA TRA CUI SCEGLIERE**********************************/
/*******************************************************************************************************/
	
	
	private void showInsCategory() {
		getAllCategories();
		TextBox name = new TextBox();
		Button insbutton = new Button("Inserisci");
		Button esci = new Button("Esci");
		HorizontalPanel hpname = new HorizontalPanel();
		HorizontalPanel hpbuttonins = new HorizontalPanel();
		VerticalPanel vpinsCategory = new VerticalPanel();
		DialogBox boxInsertCategory = new DialogBox();
		HTML br = new HTML("<br><br><br>");
		HTML inserisciCategoria = new HTML("<b>Inserisci una categoria</b>");
		
		boxInsertCategory.setText("Inserisci una categoria");
		boxInsertCategory.setAnimationEnabled(true);
		boxInsertCategory.setWidth("100%");
		boxInsertCategory.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertCategory.getElement().getStyle().setProperty("marginRight", "auto");
		
		name.getElement().setPropertyString("placeholder", "Nome categoria");
		
		hpname.add(name);
		hpbuttonins.add(insbutton);
		hpbuttonins.add(esci);
		vpinsCategory.add(inserisciCategoria);
		vpinsCategory.add(hpname);
		vpinsCategory.add(hpbuttonins);
		boxInsertCategory.add(vpinsCategory);
		
		RootPanel.get().add(br);
		RootPanel.get().add(boxInsertCategory);
		showAllSubandCategories();
		
		insbutton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				if(name.getText().equals("")) {
					Window.alert("Il campo testo non deve essere vuoto.");
				} else {
					sendInsertCategory(name.getText());
				}
			}
		});
		
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showSchermata();
			}
		});	
	}
	
	private void showInsSubCategory() {
		TextBox name = new TextBox();
		Button insbutton = new Button("Inserisci");
		Button esci = new Button("Esci");
		HorizontalPanel hpname = new HorizontalPanel();
		HorizontalPanel hpCatMadre = new HorizontalPanel();
		HorizontalPanel hpbuttonins = new HorizontalPanel();
		VerticalPanel vpinsCategory = new VerticalPanel();
		DialogBox boxInsertCategory = new DialogBox();
		HTML br = new HTML("<br><br><br>");
		HTML inserisciCategoria = new HTML("<b>Inserisci una sottocategoria</b>");
		
		boxInsertCategory.setText("Inserisci una sottocategoria");
		boxInsertCategory.setAnimationEnabled(true);
		boxInsertCategory.setWidth("100%");
		boxInsertCategory.getElement().getStyle().setProperty("marginLeft", "auto");
		boxInsertCategory.getElement().getStyle().setProperty("marginRight", "auto");
		HTML textName = new HTML("<div style=\"font-size:16px;\">Categoria: </div>");
		name.getElement().setPropertyString("placeholder", "Nome categoria");
		
		
		final HorizontalPanel categoryPanel = new HorizontalPanel();
		HTML textCategory = new HTML("<div style=\"font-size:16px;\">Categoria Madre: </div>");
		ListBox lb = new ListBox();
		
		//metodo che cerca tutte le categorie che poi verranno stampate nella listbox
		//devo cercare le categorie ma anche le sottocategorie
		gtasync.cercaCategoria(new AsyncCallback<ArrayList<Categoria>>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<Categoria> result) {
				for(int i = 0; i<result.size(); i++) {
					lb.addItem(result.get(i).getNome());
				}
			}
		});
	
		categoryPanel.add(textCategory);
		gtasync.cercaSottoCategoria(new AsyncCallback<ArrayList<SottoCategoria>>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<SottoCategoria> result) {
				for(int i = 0; i<result.size(); i++) {
					lb.addItem(result.get(i).getNome());
				}
			}
		});
		categoryPanel.add(lb);
		lb.setVisibleItemCount(1);
		
		hpname.add(textName);
		hpname.add(name);
		hpCatMadre.add(categoryPanel);
		hpbuttonins.add(insbutton);
		hpbuttonins.add(esci);
		vpinsCategory.add(inserisciCategoria);
		vpinsCategory.add(hpname);
		vpinsCategory.add(hpCatMadre);
		vpinsCategory.add(hpbuttonins);
		boxInsertCategory.add(vpinsCategory);
		
		RootPanel.get().add(br);
		RootPanel.get().add(boxInsertCategory);
		
		
		insbutton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				if(name.getText().equals("")) {
					Window.alert("Il campo testo non deve essere vuoto.");
				} else {
					String categoriaScelta = lb.getSelectedItemText();
					sendInsertSubCategory(name.getText(), categoriaScelta);
				}
				
			}
		});
		
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showSchermata();
			}
		});
		
	}
	
	private void showRenameCategory() {
		TextBox newname = new TextBox();
		Button renamebutton = new Button("Inserisci");
		Button esci = new Button("Esci");
		HorizontalPanel hpoldname = new HorizontalPanel();
		HorizontalPanel hpnewname = new HorizontalPanel();
		HorizontalPanel hpbuttonrename = new HorizontalPanel();
		VerticalPanel vprenameCategory = new VerticalPanel();
		DialogBox boxRenameCategory = new DialogBox();
		HTML br = new HTML("<br><br><br>");
		//HTML rinominaCategoria = new HTML("<b>Rinomina una categoria/sottocategoria</b>");
		
		final HorizontalPanel categoryPanel = new HorizontalPanel();
		HTML textCategory = new HTML("<div style=\"font-size:16px;\">Vecchia categoria: </div>");
		ListBox lb = new ListBox();
		
		//metodo che cerca tutte le categorie che poi verranno stampate nella listbox
		//devo cercare le categorie ma anche le sottocategorie
		gtasync.cercaCategoria(new AsyncCallback<ArrayList<Categoria>>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<Categoria> result) {
				for(int i = 0; i<result.size(); i++) {
					lb.addItem(result.get(i).getNome());
				}
			}
		});
	
		categoryPanel.add(textCategory);
		gtasync.cercaSottoCategoria(new AsyncCallback<ArrayList<SottoCategoria>>() {

			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}
			public void onSuccess(ArrayList<SottoCategoria> result) {
				for(int i = 0; i<result.size(); i++) {
					lb.addItem(result.get(i).getNome());
				}
			}
		});
		categoryPanel.add(lb);
		lb.setVisibleItemCount(1);
		
		
		boxRenameCategory.setText("Rinomina una categoria/sottocategoria");
		boxRenameCategory.setAnimationEnabled(true);
		boxRenameCategory.setWidth("100%");
		boxRenameCategory.getElement().getStyle().setProperty("marginLeft", "auto");
		boxRenameCategory.getElement().getStyle().setProperty("marginRight", "auto");
		
		//oldname.getElement().setPropertyString("placeholder", "Nome attuale categoria");
		HTML textNewCategory = new HTML("<div style=\"font-size:16px;\">Nuova categoria: </div>");
		newname.getElement().setPropertyString("placeholder", "Nuovo nome categoria");
		
		//hpoldname.add(oldname);
		hpnewname.add(textNewCategory);
		hpnewname.add(newname);
		hpbuttonrename.add(renamebutton);
		hpbuttonrename.add(esci);
		vprenameCategory.add(categoryPanel);
		vprenameCategory.add(hpoldname);
		vprenameCategory.add(hpnewname);
		vprenameCategory.add(hpbuttonrename);
		boxRenameCategory.add(vprenameCategory);
		
		RootPanel.get().add(br);
		RootPanel.get().add(boxRenameCategory);
		
		/*PER MICHELE: ELIMINARE LA RIGA SUBITO QUI SOTTO DOPO AVER INSERITO IL MENU A TENDINA PER LA CATEGORIA
		 *ELIMINARE ANCHE QUESTO COMMENTO DOPO AVER ELIMINATO LA RIGA*/
		showAllSubandCategories();
		
		renamebutton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				if(newname.getText().equals("")) {
					Window.alert("Il campo testo non deve essere vuoto.");
				} else {
					String categoriaScelta = lb.getSelectedItemText();
					sendRenameSubORCategory(categoriaScelta,newname.getText());
				}
				
			}
		});
		
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showSchermata();
			}
		});
		
	}
	
	private void showRemoveObject() {
		
		DialogBox boxRemoveObject = new DialogBox();
		HTML br = new HTML("<br><br><br>");
		
		boxRemoveObject.setText("Inserisci l'id dell'oggetto da eliminare");
		boxRemoveObject.setAnimationEnabled(true);
		boxRemoveObject.setWidth("100%");
		boxRemoveObject.getElement().getStyle().setProperty("marginLeft", "auto");
		boxRemoveObject.getElement().getStyle().setProperty("marginRight", "auto");
	
		boxRemoveObject.add(showAllObjects());
		
		RootPanel.get().add(br);
		RootPanel.get().add(boxRemoveObject);
		
	}
	
	private void showRemoveAnswers() {

		DialogBox boxRemoveAnswers = new DialogBox();
		HTML br = new HTML("<br><br><br>");
		
		boxRemoveAnswers.setText("Inserisci l'id della risposta da eliminare");
		boxRemoveAnswers.setAnimationEnabled(true);
		boxRemoveAnswers.setWidth("100%");
		boxRemoveAnswers.getElement().getStyle().setProperty("marginLeft", "auto");
		boxRemoveAnswers.getElement().getStyle().setProperty("marginRight", "auto");

		boxRemoveAnswers.add(showAllAnswers());
		
		RootPanel.get().add(br);
		RootPanel.get().add(boxRemoveAnswers);
		
	}
	
	private void showRemoveQuestion() {

		DialogBox boxRemoveQuestion = new DialogBox();
		HTML br = new HTML("<br><br><br>");
		
		boxRemoveQuestion.setText("Inserisci l'id della domanda da eliminare");
		boxRemoveQuestion.setAnimationEnabled(true);
		boxRemoveQuestion.setWidth("100%");
		boxRemoveQuestion.getElement().getStyle().setProperty("marginLeft", "auto");
		boxRemoveQuestion.getElement().getStyle().setProperty("marginRight", "auto");
		
		boxRemoveQuestion.add(showAllQuestions());
		
		RootPanel.get().add(br);
		RootPanel.get().add(boxRemoveQuestion);
		
	}
	

	private void showDeleteOfferta() {

		DialogBox boxDeleteOffert = new DialogBox();
		HTML br = new HTML("<br><br><br>");
		
		boxDeleteOffert.setText("Rinomina una categoria/sottocategoria");
		boxDeleteOffert.setAnimationEnabled(true);
		boxDeleteOffert.setWidth("100%");
		boxDeleteOffert.getElement().getStyle().setProperty("marginLeft", "auto");
		boxDeleteOffert.getElement().getStyle().setProperty("marginRight", "auto");
		
		boxDeleteOffert.add(showAllOfferts());
		
		RootPanel.get().add(br);
		RootPanel.get().add(boxDeleteOffert);

	}

	
/*******************************************************************************************************/
/*******************METODI SEND PER INVIARE IL METODO DAL CLIENT AL SERVER******************************/
/*******************************************************************************************************/
	
	
	private void sendInsertCategory(String nomeCategoria) {
		gtasync.insertCategory(nomeCategoria, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			@Override
			public void onSuccess(String result) {
				Window.alert(result);
				showSchermata();
			}
			
		});
		
	}
	
	private void sendRemoveObject(Oggetto o) {
		gtasync.removeObject(o, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			@Override
			public void onSuccess(String result) {
				Window.alert(result);
				showSchermata();
			}
			
		});
		
	}
	
	private void sendInsertSubCategory(String nomeCategoria, String categoriaMadre) {
		gtasync.insertSubCategory(nomeCategoria,categoriaMadre, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			@Override
			public void onSuccess(String result) {
				Window.alert(result);
				showSchermata();
			}
			
		});
		
	}
	
	private void sendRenameSubORCategory(String oldname, String newname) {
		gtasync.renameCategory(oldname,newname, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			@Override
			public void onSuccess(String result) {
				Window.alert(result);
				showSchermata();
			}
			
		});
		
	}
	
	
	private void sendDeleteOffert(Offerta off) {
		gtasync.deleteOffert(off, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			@Override
			public void onSuccess(String result) {
				Window.alert(result);
				showSchermata();
			}
			
		});
		
	}
	
	private void sendDeleteQuestion(Domanda d) {
		gtasync.removeDomanda(d, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			@Override
			public void onSuccess(String result) {
				Window.alert(result);
				showSchermata();
			}
			
		});
		
	}
	
	private void sendDeleteAnswer(Risposta r) {
		gtasync.removeRisposta(r, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);
			}

			@Override
			public void onSuccess(String result) {
				Window.alert(result);
				showSchermata();
			}
			
		});
		
	}
	
	
/*******************************************************************************************************/
/**************METODI GET PER PRENDERE GLI OGGETTI DA FAR VISUALIZZARE PER LA SCELTA********************/
/*******************************************************************************************************/
	
	
	public void getAllOfferts(){
		gtasync.allOffert(new AsyncCallback<ArrayList<Offerta>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);	
			}
	
			@Override
			public void onSuccess(ArrayList<Offerta> result) {
				offerts=result;	
			}	
		});
	}
	
	
	public void getAllCategories(){
		gtasync.cercaCategoria(new AsyncCallback<ArrayList<Categoria>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);	
			}

			@Override
			public void onSuccess(ArrayList<Categoria> result) {
				categories=result;	
			}	
		});
	}
	
	
	public void getAllSubCategories(){
		gtasync.cercaSottoCategoria(new AsyncCallback<ArrayList<SottoCategoria>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);	
			}

			@Override
			public void onSuccess(ArrayList<SottoCategoria> result) {
				subcategories=result;	
			}	
		});
	}
	
	
	public void getAllObjects(){
		gtasync.allObjects(new AsyncCallback<ArrayList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);	
			}

			public void onSuccess(ArrayList<Oggetto> result) {
				objects=result;	
			}	
		});
	}
	
	
	public void getAllQuestions(){
		gtasync.allQuestions(new AsyncCallback<ArrayList<Domanda>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);	
			}

			public void onSuccess(ArrayList<Domanda> result) {
				questions=result;	
			}	
		});
	}
	
	
	public void getAllAnswers(){
		gtasync.allAnswers(new AsyncCallback<ArrayList<Risposta>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(SERVER_ERROR);	
			}

			public void onSuccess(ArrayList<Risposta> result) {
				answers=result;	
			}	
		});
	}
	
	
/*******************************************************************************************************/
/******************METODI SHOW PER VISUALIZZARE GLI OGGETTI PRESI DAI METODI GET************************/
/*******************************************************************************************************/
	
	
	private void showAllSubandCategories() {
		HTML br=new HTML("<br><br>");
		HTML title=new HTML("<b style=\"font-size:18px; \">Categorie Presenti</b><br><br><br>");
		VerticalPanel categoryandsub = new VerticalPanel();
		categoryandsub.getElement().getStyle().setProperty("marginLeft", "auto");
		categoryandsub.getElement().getStyle().setProperty("marginRight", "auto");
		categoryandsub.add(title);
	
		for(int i=0;i<categories.size();i++) {
			HTML category=new HTML("<div style=\"font-size:16px;\">"+categories.get(i).getNome()+"</div>");
			categoryandsub.add(category);
		}
		for(int i=0;i<subcategories.size();i++) {
			HTML category=new HTML("<div style=\"font-size:16px;\">"+subcategories.get(i).getCategoriaMadre()+subcategories.get(i).getNome()+"</div>");
			categoryandsub.add(category);
		}
		RootPanel.get().add(br);
		RootPanel.get().add(categoryandsub);
	}
	
	private VerticalPanel showAllOfferts() {
		HTML br=new HTML("<br><br>");
		HTML title=new HTML("<b style=\"font-size:18px; \">Offerte Presenti</b><br><br><br>");
		
		VerticalPanel vpofferts = new VerticalPanel();
		vpofferts.getElement().getStyle().setProperty("marginLeft", "auto");
		vpofferts.getElement().getStyle().setProperty("marginRight", "auto");
		vpofferts.add(title);
		
		for(int i=0;i<offerts.size();i++) {
			final int k=i;
			Button remove=new Button("Elimina",new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					sendDeleteOffert(offerts.get(k));
				}
			});
			HTML offert=new HTML("<div style=\"font-size:16px;\">Offerente: "+offerts.get(i).getOfferente()+"<br>Nome dell'oggetto dell'offerta: "+offerts.get(i).getOggetto().getNome()+"</div>");
			HorizontalPanel hpofferts = new HorizontalPanel();
			hpofferts.add(offert);
			hpofferts.add(remove);
			vpofferts.add(hpofferts);
		}
		Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showSchermata();
			}
		});
		vpofferts.add(esci);
		return vpofferts;
	}
	
	private VerticalPanel showAllObjects() {
		HTML br=new HTML("<br><br>");
		HTML title=new HTML("<b style=\"font-size:18px; \">Oggetti Presenti</b><br><br><br>");

		VerticalPanel vpobjects = new VerticalPanel();
		vpobjects.getElement().getStyle().setProperty("marginLeft", "auto");
		vpobjects.getElement().getStyle().setProperty("marginRight", "auto");
		vpobjects.add(title);
		
		for(int i=0;i<objects.size();i++) {
			final int k=i;
			Button remove=new Button("Elimina",new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					sendRemoveObject(objects.get(k));
				}
			});
			HTML object=new HTML("<div style=\"font-size:12px;\">Nome dell'oggetto: "+objects.get(i).getNome()+"</div>");
			HorizontalPanel hpobjects = new HorizontalPanel();
			hpobjects.add(object);
			hpobjects.add(remove);
			vpobjects.add(hpobjects);
		}
		Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showSchermata();
			}
		});
		vpobjects.add(esci);
		
		return vpobjects;
	}
	
	private VerticalPanel showAllQuestions() {
		HTML br=new HTML("<br><br>");
		HTML title=new HTML("<b style=\"font-size:18px; \">Domande Presenti</b><br><br><br>");
		VerticalPanel vpQuestions = new VerticalPanel();
		vpQuestions.getElement().getStyle().setProperty("marginLeft", "auto");
		vpQuestions.getElement().getStyle().setProperty("marginRight", "auto");
		vpQuestions.add(title);
		
		for(int i=0;i<questions.size();i++) {
			final int k=i;
			Button remove=new Button("Elimina",new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					sendDeleteQuestion(questions.get(k));
				}
			});
			HTML object=new HTML("<div style=\"font-size:16px;\">Testo: "+questions.get(i).getQuestion()+"</div>");
			HorizontalPanel hpQuestions = new HorizontalPanel();
			hpQuestions.add(object);
			hpQuestions.add(remove);
			vpQuestions.add(hpQuestions);
		}
		Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showSchermata();
			}
		});
		vpQuestions.add(esci);
		return vpQuestions;
	}
	
	private VerticalPanel showAllAnswers() {
		HTML br=new HTML("<br><br>");
		HTML title=new HTML("<b style=\"font-size:18px; \">Risposte Presenti</b><br><br><br>");
		VerticalPanel vpanswers = new VerticalPanel();
		vpanswers.getElement().getStyle().setProperty("marginLeft", "auto");
		vpanswers.getElement().getStyle().setProperty("marginRight", "auto");
		vpanswers.add(title);
		
		for(int i=0;i<answers.size();i++) {
			final int k=i;
			Button remove=new Button("Elimina",new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					sendDeleteAnswer(answers.get(k));
				}
			});
			HTML object=new HTML("<div style=\"font-size:16px;\">Domanda: "+answers.get(i).getInstanceDomanda().getQuestion()+"<br>Risposta: "+answers.get(i).getAnswer()+"</div>");
			HorizontalPanel hpanswers = new HorizontalPanel();
			hpanswers.add(object);
			hpanswers.add(remove);
			vpanswers.add(hpanswers);
		}
		Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				showSchermata();
			}
		});
		vpanswers.add(esci);
		return vpanswers;
	}
	

}
