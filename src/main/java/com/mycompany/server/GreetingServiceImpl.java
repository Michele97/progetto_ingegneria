package com.mycompany.server;

import java.io.File;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;


import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.HTreeMap.KeySet;
import org.mapdb.Serializer;
import com.mycompany.shared.*;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.ibm.icu.text.SimpleDateFormat;
import com.mycompany.client.GreetingService;
import com.mycompany.server.serializerobjectprog.*;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import com.mycompany.shared.*;

@SuppressWarnings({ "serial", "unused" })
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
	public String offerenteMaggiore = "";
	public static final String DB_FILE = "database.db";
	public static final String SIMPLE_MAP = "simpleMap";
	private final Admin admin = new Admin();
	Boolean check=true;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

	//metodo che crea il db, nel caso non esista, e, se nel caso esistesse, lo richiama
	private DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.fileDB(DB_FILE).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);
				creaCategorie();
			}

			if(check){
				check=false;
				concludiAsta();
			}

			return db;
		}
	}


	//metodo per la registrazione
	public String register(String username, String password, String nome, String cognome, String telefono, String email, String codiceFiscale, String indirizzo, String sesso, String dataNascita, String luogoNascita) throws IllegalArgumentException {

		DB db = getDB();
		String esito ="";

		if(username.isEmpty() || password.isEmpty() || nome.isEmpty() || nome.isEmpty() || cognome.isEmpty() || telefono.isEmpty() || email.isEmpty() || codiceFiscale.isEmpty() || indirizzo.isEmpty()){
			return "Vanno compilati tutti i campi! Il prezzo deve essere maggiore di 0!";
		}

		//inizializzo l'utente da inserire nel db
		Utente u = new Utente.UtenteBuilder(username,  password,  nome,  cognome,  telefono,  email,  codiceFiscale,  indirizzo)
				.setSesso(sesso)
				.setDataNascita(dataNascita)
				.setLuogoNascita(luogoNascita).build();
		//creo la map per gli utenti
		HTreeMap<Integer, Utente> map = db.hashMap("Utenti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerUtente()).createOrOpen();

		//setto le chiavi del db utenti per scorrerlo
		Set<Integer> keys = map.keySet();
		Boolean ok= true;


		//controllo che l'username non sia già stato inserito nel db oppure che non sia uguale a sconosciuto perchè la usiamo come stringa di
		//controllo nella homepage oppure che non sia uguale ad admin che è l'user dell'amministratore
		for(Object k : keys){
			if(map.get(k).getUsername().equals(username) || map.get(k).getUsername().equals("sconosciuto") || map.get(k).getUsername().equals("admin")){
				ok=false;
				esito="Utente già registrato!";
			}
		}
		//se l'user è unico inserisco nel db il nuovo utente
		if(ok){
			map.put(u.hashCode(), u);
			esito="Utente registrato con successo!";
			//return esito;
		}
		return esito;
	}

	//metodo per fare il login
	public String login(String username, String password) throws IllegalArgumentException {
		//richiamo il db
		DB db = getDB();
		//creo il map per gli utenti per andare a cercare l'utente corrispondente a username e password
		HTreeMap<Integer, Utente> map = db.hashMap("Utenti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerUtente()).createOrOpen();

		String result="";

		//login nel caso dell'amministratore
		if(admin.getUsername().equals(username) && admin.getPassword().equals(password)) {
			result="admin";
		}

		//setto le chiavi per cercare nel map degli utenti
		@SuppressWarnings("unchecked")
		Set<Integer> keys = map.keySet();

		//cerco l'utente corrispondente a username e password
		for (Object k : keys) {

			//se corrisponde assegno a result l'username in modo tale da passarlo poi alla schermata utente
			if (map.get(k).getUsername().equals(username) && map.get(k).getPassword().equals(password)) {
				result=map.get(k).getUsername();
			}
		}
		//se non corrispone restituisco il messaggio di errore;
		if (result.equals("")){
			result = "us o pw errati";
		}

		return result;

	}

	//metodo per inserire un oggetto da vendere
	public String vendiOggetto(String proprietario, String categoria, String nome, String dataPubblicazione, String dataScadenza, Double prezzo, String descrizione) {
		//richiamo il db
		DB db = getDB();
		String result="";
		//se i campi non sono vuoti procedo ad inserire l'oggetto
		if(!proprietario.isEmpty() && !categoria.isEmpty() && !nome.isEmpty() && !dataPubblicazione.isEmpty() && !dataScadenza.isEmpty() && !descrizione.isEmpty() && prezzo!=0.0){
			// creo una chiave univoca per l'oggetto
			String code = UUID.randomUUID().toString();
			// istanzio l'oggetto da inserire
			Oggetto o = new Oggetto.OggettoBuilder(proprietario, categoria, nome,  dataPubblicazione,  dataScadenza,  prezzo,  descrizione).setId(code).setStato("Asta in corso").build();

			//creo o richiamo la map per gli oggetti e delle categorie
			HTreeMap<Integer, Oggetto> oggetto = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();
			HTreeMap<Integer, Categoria> mapC = db.hashMap("Categorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerCategoria()).createOrOpen();
			HTreeMap<Integer, SottoCategoria> mapSC = db.hashMap("SottoCategorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerSottoCategoria()).createOrOpen();
			Boolean ok =true;
			Set<Integer> keysC = mapC.keySet();
			Set<Integer> keysSC = mapSC.keySet();
			for(Object k : keysC){
				if(mapC.get(k).getNome().equals(categoria)) {
					ok=false;
				}
			}
			for(Object k : keysSC){
				if(mapSC.get(k).getNome().equals(categoria)) {
					ok=false;
				}
			}

			if(ok){
				result="La categoria non è presente nel DB";
			} else {
				oggetto.put(o.hashCode(),o);
				result= "Oggetto inserito correttamente!";
			}

		}else{
			result= "Vanno compilati tutti i campi! Il prezzo deve essere maggiore di 0!";
		}
		return result;

	}

	//metodo che verrà richiamato dalla home page per stampare tutti gli oggetti presenti nel DB


	///visualizzare anche le domande e le risposte
	public ArrayList<Oggetto> showAllObjects(){
		DB db = getDB();
		HTreeMap<Integer, Oggetto> oggetto = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();
		@SuppressWarnings("unchecked")
		Set<Integer> keys = oggetto.keySet();
		ArrayList<Oggetto> result = new ArrayList<Oggetto>();
		for (Object k : keys) {
			result.add(oggetto.get(k));
		}
		// ORDINO GLI OGGETTI IN BASE ALLA DATA DI SCADENZA
		Collections.sort(result, new Comparator<Oggetto>() {
			//override di compare sotto l'interfaccia Comparator,
			//compare implementato in Oggetto.java
			
			public int compare(Oggetto o1, Oggetto o2) {
				Date dataScad = new Date();
				Date dataScadOgg = new Date();
				try {
					dataScad = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(o1.getDataScadenza());
					dataScadOgg= new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(o2.getDataScadenza()); 
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return dataScad.compareTo(dataScadOgg);
			}
		});
		return result;
	}


	//metodo per fare la ricerca dell'oggetto
	public ArrayList<Oggetto> cercaOggetto(String nomeOggetto) throws IllegalArgumentException {
		//richiamo il db
		DB db = getDB();

		//creo il map per gli utenti per andare a cercare l'utente corrispondente a username e password
		HTreeMap<Integer, Oggetto> oggetto = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();

		//setto le chiavi per cercare nel map degli oggetti
		@SuppressWarnings("unchecked")
		Set<Integer> keys = oggetto.keySet();
		//Boolean trovato = false;
		ArrayList<Oggetto> result = new ArrayList<Oggetto>();
		int i=0;
		//cerco gli oggetti che possiedono il nome di quell'oggetto
		for (Object k : keys) {
			//se corrisponde assegno a result l'username in modo tale da passarlo poi alla schermata utente
			if (oggetto.get(k).getNome().matches("(.*)"+nomeOggetto+"(.*)")) {
				if(oggetto.get(k).getStato().equals("Asta in corso")){
					result.add(oggetto.get(k));
				}
			}
		}

		return result;

	}

	//metodo che aggiunge le 5 categorie di default richieste dalle specifiche
	public void creaCategorie() throws IllegalArgumentException {
		DB db = getDB();

		Categoria c1 = new Categoria("Abbigliamento");
		Categoria c2 = new Categoria("Casa");
		Categoria c3 = new Categoria("Elettronica");
		Categoria c4 = new Categoria("Giardinaggio");
		Categoria c5 = new Categoria("Sport");

		HTreeMap<Integer, Categoria> mapC = db.hashMap("Categorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerCategoria()).createOrOpen();


		Categoria [] cat = {c1,c2,c3,c4,c5};

		//controllo che le categorie non siano gia presenti ne db
		Set<Integer> keys = mapC.keySet();
		for (int i=0; i<5; i++) {
			Boolean ok=false;
			for (Object k : keys) {
				if(cat[i].getNome().equals(mapC.get(k).getNome())){
					ok=true;
				}
			}
			if(!ok){
				mapC.put(cat[i].hashCode(),cat[i]);
			}
		}
	}
	//metodo che trova il l'offerta con l'importo massimo, dato un oggetto
	public Double maxPrezzo(String idOggetto){
		DB db = getDB();
		HTreeMap<Integer, Offerta> offerta = db.hashMap("Offerte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOfferta()).createOrOpen();
		Set<Integer> keys = offerta.keySet();
		Double maxImportoInserito = 0.0;
		//ciclo che scorre tutti gli elementi e trova il massimo
		//se non ci sono offerte inserite, l'esecuzione di questo for non produrrà nulla
		for (Object k : keys) {
			String idOggettoInTabellaOfferta = offerta.get(k).getOggetto().getId();
			if((idOggettoInTabellaOfferta.equals(idOggetto)) && (offerta.get(k).getImporto()>maxImportoInserito)){
				//aggiorno subito il massimo importo inserito, vale a dire importo raggiunto
				maxImportoInserito = offerta.get(k).getImporto();
			}
		}
		return maxImportoInserito;

	}
	//BISOGNA AGGIUNGERE IL CONTROLLO DELLO STATO DELL'OGGETTO PER VERIFICARE SE L'ASTA È IN CORSO
	//MANCA IL CONTROLLO SULL'ID DELL'OGGETTO
	public String faiOfferta(String offerente, Double importoInserito, Oggetto ogg){
		DB db = getDB();
		HTreeMap<Integer, Offerta> offerta = db.hashMap("Offerte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOfferta()).createOrOpen();
		String esito="";
		Set<Integer> keys = offerta.keySet();
		//inserisco subito l'oggetto nella tabella delle offerte per poi tenerne traccia per il futuro


		//massimo importo inserito significa il massimo importo raggiunto per quell oggetto
		Double maxImportoInserito = 0.0;
		maxImportoInserito = maxPrezzo(ogg.getId());
		if(importoInserito>maxImportoInserito) {
			for(Integer k : keys){
				//AGGIUNTO CONTROLLO PER AGGIORNARE SOLO LE OFFERTE RIFERITO A UNO SPECIFICO OGGETTO E NON  TUTTE QUANTE
				if(offerta.get(k).getOggetto().equals(ogg)){
					Offerta off=new Offerta(offerta.get(k).getOfferente(),offerta.get(k).getImporto(), offerta.get(k).getOggetto(), "asta in corso e offerta superata",offerta.get(k).getIdOfferta());

					offerta.replace(k, off);

				}
			}
			Boolean ok=true;
			for(Integer k : keys){
				// AGGIUNTO CONTROLLO PER POTER AGGIORNARE SOLO L'OFFERTA RIFERITA AD UNO SPECIFICO OGGETTO E NON PER TUTTI GLI OGGETTI PER CUI L'OFFERENTE HA FATTO UN OFFERTA
				if(offerta.get(k).getOfferente().equals(offerente) && offerta.get(k).getOggetto().equals(ogg)){
					offerta.replace(k, new Offerta(offerta.get(k).getOfferente(),importoInserito, offerta.get(k).getOggetto(), "asta in corso e offerta migliore",offerta.get(k).getIdOfferta()));
					System.out.println("Oggetto rimpiazzato "+offerta.get(k).getOfferente()+" "+offerta.get(k).getOggetto()+" "+offerta.get(k).getImporto());
					ok=false;
				}

			}
			if(ok){
				String code = UUID.randomUUID().toString();
				Offerta o = new Offerta(offerente, importoInserito, ogg, "asta in corso e offerta migliore",code);
				offerta.put(o.hashCode(),o);
				//System.out.println("Nuovo oggetto inserito "+offerta.get(o.hashCode()).getOfferente()+" "+offerta.get(o.hashCode()).getOggetto()+" "+offerta.get(o.hashCode()).getImporto());
			}

			return esito="ok";

		}else {
			return esito="Offerta non inserita.";
		}
	}


	public String faiDomanda(Oggetto ogg, String question, String mittente) throws IllegalArgumentException{

		DB db = getDB();
		HTreeMap<Integer, Domanda> domanda = db.hashMap("Domande").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerDomanda()).createOrOpen();
		String esito="";
		String id = UUID.randomUUID().toString();
		Domanda q = new Domanda(ogg, question, mittente,id);
		domanda.put(q.hashCode(), q);
		esito="Domanda inserita corretamente!"/*+domanda.get(q.hashCode()).getAnswer()+"\n"+domanda.get(q.hashCode()).getQuestion()+"\n"+domanda.get(q.hashCode()).getIdOggetto()+"\n"+domanda.get(q.hashCode()).getMittente()+"\n"+domanda.get(q.hashCode()).getDestinatario()*/;
		return esito;
	}


	public String rispondi(Domanda d, String risp) throws IllegalArgumentException{

		DB db = getDB();
		HTreeMap<Integer, Risposta> risposta = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		String esito="";
		Set<Integer> keys = risposta.keySet();
		Boolean ok = true;

		for(Object k : keys){
			if(risposta.get(k).getInstanceDomanda().equals(d)){

				esito="Hai già risposto a questa domanda!";
				ok=false;
			}
		}

		if(ok){
			String code = UUID.randomUUID().toString();
			Risposta r = new Risposta(risp,d,code);
			risposta.put(r.hashCode(), r);
			esito="Risposta inviata!";

		}

		return esito;
	}


	//prendere solo le domande che hanno ricevuto risposta
	public ArrayList<Domanda> cercaDomande(String username){

		DB db = getDB();
		HTreeMap<Integer, Domanda> domanda = db.hashMap("Domande").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerDomanda()).createOrOpen();
		HTreeMap<Integer, Risposta> risposta = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		ArrayList<Domanda> result = new ArrayList<Domanda>();
		Set<Integer> keysDomande = domanda.keySet();
		Set<Integer> keysRisposte = risposta.keySet();
		Boolean ok=true;

		for (Object k : keysDomande) {
			//result.add(domanda.get(k));
			if(domanda.get(k).getOggettoDomanda().getProprietario().equals(username)){
				ok=true;
				for (Object i : keysRisposte) {
					if(domanda.get(k).equals(risposta.get(i).getInstanceDomanda())){
						ok=false;
					}
				}
				if(ok){
					result.add(domanda.get(k));
				}
			}
		}
		return result;
	}

	public ArrayList<String> domandeRisposte(Oggetto o){

		DB db = getDB();
		HTreeMap<Integer, Domanda> domanda = db.hashMap("Domande").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerDomanda()).createOrOpen();
		HTreeMap<Integer, Risposta> risposta = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		ArrayList<String> result = new ArrayList<String>();
		Set<Integer> keysDomande = domanda.keySet();
		Set<Integer> keysRisposte = risposta.keySet();
		Boolean ok = true;

		for (Object k : keysDomande){
			if(domanda.get(k).getOggettoDomanda().equals(o)){
				for (Object i : keysRisposte){
					if(domanda.get(k).equals(risposta.get(i).getInstanceDomanda())){
						result.add("DOMANDA: "+risposta.get(i).getInstanceDomanda().getQuestion()+"<br>"+"RISPOSTA: "+risposta.get(i).getAnswer()+"</br>");
						ok=false;
					}
				}
				if(ok){
					result.add("DOMANDA: "+domanda.get(k).getQuestion());
				}
			}
		}

		return result;
	}


	//metodo che ritorna tutte le risposte relative ad un username
	public ArrayList<Risposta> cercaRisposte(String username){

		DB db = getDB();
		HTreeMap<Integer, Risposta> risposta = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		ArrayList<Risposta> result = new ArrayList<Risposta>();
		Set<Integer> keys = risposta.keySet();

		for (Object k : keys) {
			if(risposta.get(k).getInstanceDomanda().getMittente().equals(username)){
				result.add(risposta.get(k));
			}
		}
		return result;
	}

	//metodo che ritorna tutte le categorie presenti nel db
	public ArrayList<Categoria> cercaCategoria(){

		DB db = getDB();
		HTreeMap<Integer, Categoria> categoria = db.hashMap("Categorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerCategoria()).createOrOpen();
		ArrayList<Categoria> result = new ArrayList<Categoria>();
		Set<Integer> keys = categoria.keySet();

		for (Object k : keys) {

			result.add(categoria.get(k));
		}

		return result;
	}

	//metodo che ritorna tutte le sottocategorie presenti nel db
	public ArrayList<SottoCategoria> cercaSottoCategoria(){

		DB db = getDB();
		HTreeMap<Integer, SottoCategoria> sottoCategoria = db.hashMap("SottoCategorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerSottoCategoria()).createOrOpen();
		ArrayList<SottoCategoria> result = new ArrayList<SottoCategoria>();
		Set<Integer> keys = sottoCategoria.keySet();
		for (Object k : keys) {
			result.add(sottoCategoria.get(k));
		}

		return result;
	}

	//ritorno l'utente corrispondente all'username che ha fatto l'accesso per mostrare le sue informazioni
	public Utente showUserInfo(String user) {
		DB db=getDB();
		HTreeMap<Integer, Utente> map = db.hashMap("Utenti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerUtente()).createOrOpen();
		Set<Integer> keys = map.keySet();
		Utente u=new Utente();
		//prendo l'utente corrspondente all'username
		for(Object k : keys){
			if(map.get(k).getUsername().equals(user)){
				u=map.get(k);
			}
		}
		return u;



	}
	//metodo che ritorna le offerte effettuate dall'username
	public ArrayList<Offerta> cercaOfferte(String username){
		DB db = getDB();
		HTreeMap<Integer, Offerta> offerta = db.hashMap("Offerte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOfferta()).createOrOpen();
		ArrayList<Offerta> result = new ArrayList<Offerta>();
		Set<Integer> keys = offerta.keySet();

		for (Object k : keys) {
			if(offerta.get(k).getOfferente().equals(username)){
				result.add(offerta.get(k));
			}
		}
		return result;
	}



	//metodo che ritorna gli oggetti messi in vendita dall' username
	public ArrayList<Oggetto> cercaTuoiOggetti (String username){
		DB db = getDB();
		HTreeMap<Integer, Oggetto> oggettiPosseduti = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();
		ArrayList<Oggetto> result = new ArrayList<Oggetto>();
		Set<Integer> keys = oggettiPosseduti.keySet();
		for (Object k : keys) {
			if(oggettiPosseduti.get(k).getProprietario().equals(username)){
				result.add(oggettiPosseduti.get(k));
			}
		}
		return result;
	}

	//metodo per inserire una nuova sottocategoria
	public String insertCategory(String nomeCategoria) {
		//stringa da ritornare
		String esito="Categoria inserita con successo.";
		DB db = getDB();
		HTreeMap<Integer, Categoria> categoria = db.hashMap("Categorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerCategoria()).createOrOpen();
		HTreeMap<Integer, SottoCategoria> sottocategoria = db.hashMap("SottoCategorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerSottoCategoria()).createOrOpen();
		Set<Integer> keysCat = categoria.keySet();
		Set<Integer> keysSottoCat = sottocategoria.keySet();
		//bool per controllare se il nome della categoria inserita esiste già
		Boolean esiste=false;
		for (Object k : keysCat) {
			if(categoria.get(k).getNome().equals(nomeCategoria)){
				//se il nome inserito per la nuova categoria corrisponde a una categoria/sotto già esistente setto il nome a true
				esiste=true;
			}
		}
		for (Object k : keysSottoCat) {
			if(sottocategoria.get(k).getNome().equals(nomeCategoria)){
				//se il nome inserito per la nuova categoria corrisponde a una categoria/sotto già esistente setto il nome a true
				esiste=true;
			}
		}
		if(!esiste) {
			Categoria c = new Categoria(nomeCategoria);
			categoria.put(c.hashCode(),c);
		} else {
			esito="Categoria già presente. Scegliere un altro nome.";
		}

		return esito;
	}

	//metodo per inserire una nuova sottocategoria
	public String insertSubCategory(String nomeCategoria, String categoriaMadre) {
		DB db = getDB();
		//stringa da ritornare
		String esito="Sottocategoria inserita con successo.";
		//stringa da inserire nel caso in cui la madre inserita fosse una categoria
		String nameCatMadreToAdd="";
		//stringa da inserire nel caso in cui la stringa madre inserita fosse una sottocategoria
		String nameSottoCatMadreToAdd="";

		HTreeMap<Integer, Categoria> categoria = db.hashMap("Categorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerCategoria()).createOrOpen();
		HTreeMap<Integer, SottoCategoria> sottocategoria = db.hashMap("SottoCategorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerSottoCategoria()).createOrOpen();
		Set<Integer> keysCat = categoria.keySet();
		Set<Integer> keysSottoCat = sottocategoria.keySet();
		//bool per controllare se il nome della sottocategoria inserita esiste già
		Boolean esiste=false;
		//bool per controllare che la categoria madre inserita esista
		Boolean esisteCatMadre=false;
		//bool per controllare se la sottocategoria madre della nuova categoria inserita esista
		Boolean esisteSottoCatMadre=false;

		//controllo che il nome inserito non corrisponda a una categoria già esistente e se la categoriaMadre inserita sia una categoria principale
		for (Object k : keysCat) {
			if(categoria.get(k).getNome().equals(nomeCategoria)){
				//se il nome inserito per la nuova sottocategoria corrisponde a una categoria già esistente setto il nome a true
				esiste=true;
			}
			if(categoria.get(k).getNome().equals(categoriaMadre)) {
				//se esiste una categoria che possa diventare la madre della nuova sottocategoria
				esisteCatMadre=true;
				nameCatMadreToAdd=categoria.get(k).getNome()+"-";
			}
		}
		//controllo che il nome inserito non corrisponda a una sottocategoria già esistente e se la categoriaMadre inserita sia una sottocategoria
		for (Object k : keysSottoCat) {
			if(sottocategoria.get(k).getNome().equals(nomeCategoria)){
				//se il nome inserito per la nuova sottocategoria corrisponde a una sottocategoria già esistente setto il nome a true
				esiste=true;
			}
			if(sottocategoria.get(k).getNome().equals(categoriaMadre)) {
				//se esiste una sottocategoria che possa diventare la madre della nuova sottocategoria
				esisteSottoCatMadre=true;
				//aggiungo alla stringa cat madre che è fatta della concatenazione di stringhe dalla categoria principale fino al livello più basso
				//es. Casa-Salotto-Tavoli da salotto- = un esempio di una stringa categoriaMadre in SottoCategoria
				nameSottoCatMadreToAdd=sottocategoria.get(k).getCategoriaMadre()+sottocategoria.get(k).getNome()+"-";
			}
		}
		//se non esiste una sottocategoria con quel nome e la madre è una categoria
		if(!esiste && esisteCatMadre) {
			SottoCategoria c = new SottoCategoria(nomeCategoria,nameCatMadreToAdd);
			sottocategoria.put(c.hashCode(),c);
		//se non esiste una sottocategoria con quel nome e la madre è una sottocategoria
		} else if(!esiste && esisteSottoCatMadre) {
			SottoCategoria c = new SottoCategoria(nomeCategoria,nameSottoCatMadreToAdd);
			sottocategoria.put(c.hashCode(),c);
		//sennò stampo il messaggio di errore
		} else {
			esito="Categoria già presente o non esiste la categoria madre. Scegliere un altro nome.";
		}

		return esito;
	}

	public String renameCategory(String oldName, String newName) {

		DB db = getDB();
		//Stringa di ritorno
		String esito="Rinominazione completata con successo.";
		//bool per controllare se da rinominare è una sottocategoria
		Boolean sottocat=true;
		//bool per controllare se da rinominare è una categoria
		Boolean renameok=false;
		HTreeMap<Integer, Categoria> categoria = db.hashMap("Categorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerCategoria()).createOrOpen();
		HTreeMap<Integer, SottoCategoria> sottocategoria = db.hashMap("SottoCategorie").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerSottoCategoria()).createOrOpen();
		Set<Integer> keysCat = categoria.keySet();
		Set<Integer> keysSottoCat = sottocategoria.keySet();
		//stringa che sarà introdotta come nuova categoria madre
		String newCatMadre="";

		for (Integer k : keysCat) {
			//prima controllo se da rinominare è una categoria
			if(categoria.get(k).getNome().equals(oldName)){
				for (Integer x : keysSottoCat) {
					//se corrisponde creo un ciclo dove controllo tutte le sottocategorie che hanno quela categoria come madre
					if(sottocategoria.get(x).getCategoriaMadre().matches("(.*)"+oldName+"(.*)")){
						//splitto la stringa categoria madre
						String[] catMadri=sottocategoria.get(x).getCategoriaMadre().split("-");
						//controllo se una delle categoire madri corrisponde a il vecchio nome
						for(int i=0;i<catMadri.length;i++) {
							if(catMadri[i].equals(oldName)) {
								catMadri[i]=newName;
							}
							//ricostruisco la nuova stringa che formerà le categorie madri della sottocategoria
							newCatMadre+=catMadri[i]+"-";
						}
						//rimpiazzo la vecchia stringa categoria madre con la nuova rinominata
						sottocategoria.replace(k, new SottoCategoria(sottocategoria.get(x).getNome(),newCatMadre));
					}
					newCatMadre="";
				}
				categoria.replace(k, new Categoria(newName));

				//setto sottocat a false perchè so che la categoria da rinominare l'ho già trovata
				sottocat=false;
				renameok=true;
				break;
			}
		}

		//stesso ragionamento di sopra solo che se sopra non trovo il nome equivalente ad una categoria casco in questo if
		if(sottocat) {
			for (Integer k : keysSottoCat) {
				if(sottocategoria.get(k).getNome().equals(oldName)){
					for (Integer x : keysSottoCat) {
						if(sottocategoria.get(x).getCategoriaMadre().matches("(.*)"+oldName+"(.*)")){
							String[] catMadri=sottocategoria.get(x).getCategoriaMadre().split("-");
							for(int i=0;i<catMadri.length;i++) {
								if(catMadri[i].equals(oldName)) {
									catMadri[i]=newName;
								}
								newCatMadre+=catMadri[i]+"-";
							}
							sottocategoria.replace(x, new SottoCategoria(sottocategoria.get(x).getNome(),newCatMadre));
						}
						newCatMadre="";
					}
					if(newCatMadre.contentEquals("")) {
						sottocategoria.replace(k, new SottoCategoria(newName,sottocategoria.get(k).getCategoriaMadre()));
					} else {
						sottocategoria.replace(k, new SottoCategoria(newName,newCatMadre));
					}

					renameok=true;
					break;
				}
			}
		}
		//se non entro in nessuno dei due rami vuol dire che non esiste nessuna categoria con il nome inserito
		if(!renameok) {
			esito = "Nessuna categoria trovata con questo nome.";
		} else {
			HTreeMap<Integer, Oggetto> oggetti = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();
			Set<Integer> keysObjects = oggetti.keySet();
			for (Integer k : keysObjects) {
				if(oggetti.get(k).getCategoria().equals(oldName)) {
					Oggetto o=new Oggetto.OggettoBuilder(oggetti.get(k).getProprietario(), newName, oggetti.get(k).getNome(), oggetti.get(k).getDataPubblicazione(), oggetti.get(k).getDataScadenza(), oggetti.get(k).getPrezzo(), oggetti.get(k).getDescrizione()).setId(oggetti.get(k).getId()).setStato(oggetti.get(k).getStato()).build();
					oggetti.replace(k, o);
				}
			}
		}

		return esito;
	}

	//Metodo per eliminare le offerte dall'admin
	public String deleteOffert(Offerta o) {
		DB db = getDB();

		String esito="Offerta eliminata correttamente!";
		//bool per salvarmi se l'offerta c'è o no
		Boolean eliminata=false;
		//la variabile per memorizzare la K dell'offerta da eliminare
		int keyOffertToDelete=0;
		//l'id dell'oggeto corrispondente alla offerta da eliminare per vedere se lo stato è asta in corso
		String idObjectCheckStatus="";
		//bool per verificare che l'offerta da eliminare sia per un oggetto con asta in corso
		Boolean oggettoAstaInCorso=false;
		//mi prendo l'oggetto dell'offerta in modo tale da risettare l'offerta migliore
		Oggetto oggDellOfferta=o.getOggetto();

		HTreeMap<Integer, Offerta> offerta = db.hashMap("Offerte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOfferta()).createOrOpen();
		Set<Integer> keys = offerta.keySet();

		for(Integer k : keys) {
			if(offerta.get(k).equals(o)) {
				//mi salvo la K dell'offerta da eliminare
				keyOffertToDelete=k;
				//mi salvo l'id dell'oggetto dell'offerta da eliminare
				idObjectCheckStatus=offerta.get(k).getOggetto().getId();
				
				//setto il bool dell'offerta eliminata a true
				eliminata=true;
			}
		}
		//se l'offerta viene trovata controllo che l'oggetto associato sia con asta in corso
		Double maxImportoInserito = 0.0;
		if(eliminata) {
			if(o.getOggetto().getStato().equals("Asta in corso")) {
				offerta.remove(keyOffertToDelete);
				Set<Integer> keysAfterRemove = offerta.keySet();
				//ricalcolo il massimo importo offerto dopo il remove
				//controllo se ci sia una offreta con il nuovo importo più alto nel caso in cui
				//la rimossa fosse stata la migliore
				maxImportoInserito=maxPrezzo(oggDellOfferta.getId());

				for(Integer k : keysAfterRemove){
					
					if(offerta.get(k).getOggetto().equals(oggDellOfferta) && offerta.get(k).getImporto().equals(maxImportoInserito)){
						Offerta off=new Offerta(offerta.get(k).getOfferente(),offerta.get(k).getImporto(), offerta.get(k).getOggetto(), "asta in corso e offerta migliore",offerta.get(k).getIdOfferta());
						offerta.replace(k, off);

					}
					
				}
				//se è con asta in corso procedo alla eliminazione
				oggettoAstaInCorso=true;
			}
			//se non è true vuol dire che l'oggetto ha uno stato diverso da asta in corso e non si possono eliminre le offerte
			if(!oggettoAstaInCorso) {
				esito="L'oggetto selezionato ha concluso l'asta e non è possibile elimiare offerte";
			}
		//se eliminata=false allora non ho trovato nessuna offerta con i dati in input
		} else {
			esito="I dati inseriti non corrispondono a nessuna offerta";
		}

		return esito;
	}

	//metodo per listare tutte le offerte presenti sotto all'elimina offerta in admin
	public ArrayList<Offerta> allOffert(){
		DB db=getDB();

		ArrayList<Offerta> result = new ArrayList<Offerta>();
		HTreeMap<Integer, Offerta> offerta = db.hashMap("Offerte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOfferta()).createOrOpen();
		Set<Integer> keys = offerta.keySet();
		for(Integer k : keys) {
			result.add(offerta.get(k));
		}

		return result;
	}

	//Metodo per portare alla schermata admin tutti gli oggetti presenti in modo da poterne scegliere uno da eliminare
	public ArrayList<Oggetto> allObjects(){
		DB db = getDB();
		HTreeMap<Integer, Oggetto> oggetto = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();
		@SuppressWarnings("unchecked")
		Set<Integer> keys = oggetto.keySet();
		ArrayList<Oggetto> result = new ArrayList<Oggetto>();
		for (Object k : keys) {
			result.add(oggetto.get(k));
		}
		return result;
	}


	public String removeObject(Oggetto ogg) {
		DB db = getDB();
		//stringa di ritorno del metodo
		String esito="Oggetto rimosso con successo";
		//bool per controllare se l'oggetto da eliminare sia stato trovato
		Boolean objectRemoved=false;
		HTreeMap<Integer, Oggetto> oggetti = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();
		Set<Integer> keysObjects = oggetti.keySet();
		HTreeMap<Integer, Offerta> offerta = db.hashMap("Offerte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOfferta()).createOrOpen();
		Set<Integer> keysOfferts = offerta.keySet();
		HTreeMap<Integer, Domanda> domande = db.hashMap("Domande").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerDomanda()).createOrOpen();
		Set<Integer> keysQuestions = domande.keySet();
		HTreeMap<Integer, Risposta> risposte = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		Set<Integer> keysAnswers = risposte.keySet();

		for (Integer k : keysObjects) {
			//controllo se l'id passato corrispone a un oggetto presente
			System.out.println(oggetti.get(k).getId()+"    "+ogg.getId());
			if(oggetti.get(k).equals(ogg)) {
				System.out.println(oggetti.get(k).getId()+"    "+ogg.getId());
				//se corrisponde elimino tutte le domande, risposte e offerte associate a quell'oggetto
				for (Integer x : keysOfferts) {
					if(offerta.get(x).getOggetto().equals(ogg)) {
						offerta.remove(x);
					}
				}
				for (Integer x : keysQuestions) {
					if(domande.get(x).getOggettoDomanda().equals(ogg)) {
						for (Integer v : keysAnswers) {
							if(risposte.get(v).getInstanceDomanda()==(domande.get(x))) {
								risposte.remove(v);
							}
						}
						domande.remove(x);
					}
				}
				//setto objectRemoved a true perchè l'id cercato corrisponde ad un oggetto e lo rimuovo
				objectRemoved=true;
				oggetti.remove(k);
			}
		}
		//se non trovo nessun oggetto corrispondente a quell'id
		if(!objectRemoved) {
			esito="l'oggettto selezionato non esiste";
		}

		return esito;
	}

	//metodo per rimuovere una domanda
	public String removeDomanda(Domanda d) {
		DB db = getDB();
		//stringa da ritornare
		String esito="Domanda eliminata con successo";
		//bool per controllare se trovo la domanda oppure no
		Boolean find=false;
		HTreeMap<Integer, Domanda> domande = db.hashMap("Domande").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerDomanda()).createOrOpen();
		Set<Integer> keysQuestions = domande.keySet();
		HTreeMap<Integer, Risposta> risposte = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		Set<Integer> keysAnswers = risposte.keySet();

		for (Integer k : keysQuestions) {
			if(domande.get(k).equals(d)) {
				//se trovo la domanda vedo se c'è una risposta associata e la rimuovo
				for(Integer x : keysAnswers) {
					if(risposte.get(x).getInstanceDomanda().equals(d)) {
						risposte.remove(x);
					}
				}
				//rimuovo la domanda trovata e setto find=true
				domande.remove(k);
				find=true;
			}
		}
		//se non la trovo stampo non trovata
		if(!find) {
			esito="L'id non corrisponde a nessuna domanda";
		}

		return esito;
	}

	//metodo che mi ritorna tutte le domande nel db
	public ArrayList<Domanda> allQuestions(){
		DB db = getDB();
		HTreeMap<Integer, Domanda> domande = db.hashMap("Domande").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerDomanda()).createOrOpen();
		Set<Integer> keysQuestions = domande.keySet();
		ArrayList<Domanda> result = new ArrayList<Domanda>();
		for (Integer k : keysQuestions) {
			result.add(domande.get(k));
		}
		return result;
	}

	//metodo per rimuovere una risposta
	public String removeRisposta(Risposta r) {
		DB db = getDB();
		//stringa da ritornare
		String esito="Risposta eliminata con successo";
		//bool per verificare se trovo la risposta
		Boolean find=false;
		HTreeMap<Integer, Risposta> risposte = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		Set<Integer> keysAnswers = risposte.keySet();

		for(Integer x : keysAnswers) {
			if(risposte.get(x).equals(r)) {
				//se trovo la risposta associata all'id inserito la rimuovo e setto find=true
				risposte.remove(x);
				find=true;
			}
		}
		//se non la trovo find=fale e stampo non trovata
		if(!find) {
			esito="L'id non corrisponde a nessuna risposta";
		}

		return esito;
	}

	//metodo che mi stampa tutte le risposte
	public ArrayList<Risposta> allAnswers(){
		DB db = getDB();
		HTreeMap<Integer, Risposta> risposte = db.hashMap("Risposte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerRisposta()).createOrOpen();
		Set<Integer> keysAnswers = risposte.keySet();
		ArrayList<Risposta> result = new ArrayList<Risposta>();
		for (Integer k : keysAnswers) {
			result.add(risposte.get(k));
		}
		return result;
	}


	public void concludiAsta(){

		Timer t = new Timer( );
		t.scheduleAtFixedRate(new TimerTask() {//metodo che avvia un task mantenendolo sempre in ascolto senza impedire l'esecuzione di altri task

			@Override
			public void run() {

				DB db = getDB();
				HTreeMap<Integer, Oggetto> oggetto = db.hashMap("Oggetti").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOggetto()).createOrOpen();
				HTreeMap<Integer, Offerta> offerta = db.hashMap("Offerte").keySerializer(Serializer.INTEGER).valueSerializer(new SerializerOfferta()).createOrOpen();
				Set<Integer> keysOfferte = offerta.keySet();
				Set<Integer> keysOggetti = oggetto.keySet();
				LocalDateTime now = LocalDateTime.now();
				Boolean ok =true;

				for (Integer k : keysOggetti) {
					LocalDateTime dataScadenza = LocalDateTime.parse(oggetto.get(k).getDataScadenza(), formatter);

					//per ogni oggetto controllo che la data di scadenza sia stata raggiunta
					if(now.isAfter(dataScadenza)){
						for (Integer x : keysOfferte) {
							//per ogni oggetto "scaduto" controllo se ha ricevuto delle offerte
							if(offerta.get(x).getOggetto().equals(oggetto.get(k)) && offerta.get(x).getStato().equals("asta in corso e offerta migliore")){
								//se è cosi assegno l'oggetto al miglior offerente andando ad aggiornare lo stato dell'oggetto e della relativa offerte vincente
								Oggetto o = new Oggetto.OggettoBuilder(oggetto.get(k).getProprietario(), oggetto.get(k).getCategoria(), oggetto.get(k).getNome(),  oggetto.get(k).getDataPubblicazione(),  oggetto.get(k).getDataScadenza(),  oggetto.get(k).getPrezzo(),  oggetto.get(k).getDescrizione()).setId(oggetto.get(k).getId())
										.setStato("asta conclusa e oggetto aggiudicato da "+offerta.get(x).getOfferente()+" che ha offerto l'importo: "+offerta.get(x).getImporto()).build();
								oggetto.replace(k,o);
								offerta.replace(x, new Offerta(offerta.get(x).getOfferente(), offerta.get(x).getImporto(), offerta.get(x).getOggetto(), "asta chiusa e oggetto aggiudicato",offerta.get(x).getIdOfferta()));
								ok=false;
								//System.out.println(oggetto.get(k).getNome()+" -> "+oggetto.get(k).getStato()+"aggiudicato da "+offerta.get(x).getOfferente());

							}else if(offerta.get(x).getOggetto().equals(oggetto.get(k)) && offerta.get(x).getStato().equals("asta in corso e offerta superata")){
								//aggiorno anche lo stato delle offerte non vincenti, se ci sono
								offerta.replace(x, new Offerta(offerta.get(x).getOfferente(),offerta.get(x).getImporto(), offerta.get(x).getOggetto(), "asta chiusa e oggetto non aggiudicato",offerta.get(x).getIdOfferta()));

							}
						}

						if(ok && oggetto.get(k).getStato().equals("Asta in corso")){
							//Questa parte viene eseguita solo se l'oggetto non ha ricevuto nessuna offerta
							Oggetto o = new Oggetto.OggettoBuilder(oggetto.get(k).getProprietario(), oggetto.get(k).getCategoria(), oggetto.get(k).getNome(),  oggetto.get(k).getDataPubblicazione(),  oggetto.get(k).getDataScadenza(),  oggetto.get(k).getPrezzo(),  oggetto.get(k).getDescrizione()).setId(oggetto.get(k).getId())
									.setStato("asta conclusa e oggetto non aggiudicato").build();
							oggetto.replace(k,o);
							//System.out.println(oggetto.get(k).getNome()+" -> "+oggetto.get(k).getStato());

						}
					}
				}
			}


		}, 0,10000); // (parte dopo 10sec, e si ripete ogni 10 sec)

	}

}
