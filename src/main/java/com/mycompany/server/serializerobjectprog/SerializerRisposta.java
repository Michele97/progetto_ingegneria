package com.mycompany.server.serializerobjectprog;

import com.mycompany.shared.Domanda;
import com.mycompany.shared.Oggetto;
import com.mycompany.shared.Risposta;

import java.io.IOException;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

public class SerializerRisposta implements Serializer<Risposta>, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(DataOutput2 out, Risposta value) throws IOException {
		
		out.writeUTF(value.getAnswer());
		out.writeUTF(value.question.oggetto.getProprietario());
		out.writeUTF(value.question.oggetto.getCategoria());
		out.writeUTF(value.question.oggetto.getNome());
		out.writeUTF(value.question.oggetto.getDataPubblicazione());
		out.writeUTF(value.question.oggetto.getDataScadenza());
		out.writeDouble(value.question.oggetto.getPrezzo());
		out.writeUTF(value.question.oggetto.getDescrizione());
		out.writeUTF(value.question.oggetto.getId());
		out.writeUTF(value.question.oggetto.getStato());
		out.writeUTF(value.question.getQuestion());
		out.writeUTF(value.question.getMittente());
		out.writeUTF(value.question.getIdDomanda());
		out.writeUTF(value.getIdRisposta());

	}

	@Override
	public Risposta deserialize(DataInput2 input, int available) throws IOException {
		return new Risposta(input.readUTF(),
							new Domanda(new Oggetto.OggettoBuilder(input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readDouble(),input.readUTF())
								    .setId(input.readUTF()).setStato(input.readUTF()).build()
								    ,input.readUTF(),input.readUTF(),input.readUTF()),
							input.readUTF());
	}

}
