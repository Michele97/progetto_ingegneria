package com.mycompany.server.serializerobjectprog;

import java.io.IOException;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import com.mycompany.shared.Categoria;

public class SerializerCategoria implements Serializer<Categoria>, Serializable {

  private static final long serialVersionUID = 1L;

	@Override
	public void serialize(DataOutput2 out, Categoria value) throws IOException {
		out.writeUTF(value.getNome());
  }

  @Override
  public Categoria deserialize(DataInput2 input, int available) throws IOException {
    return new Categoria(input.readUTF());
  }
}
