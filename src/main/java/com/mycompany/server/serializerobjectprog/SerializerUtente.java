package com.mycompany.server.serializerobjectprog;

import com.mycompany.shared.Utente;

import java.io.IOException;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

public class SerializerUtente implements Serializer<Utente>, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(DataOutput2 out, Utente value) throws IOException {
		out.writeUTF(value.getUsername());
		out.writeUTF(value.getPassword());
		out.writeUTF(value.getNome());
		out.writeUTF(value.getCognome());
		out.writeUTF(value.getTelefono());
		out.writeUTF(value.getEmail());
		out.writeUTF(value.getCodiceFiscale());
		out.writeUTF(value.getIndirizzo());
		out.writeUTF(value.getSesso());
		out.writeUTF(value.getDataNascita());
		out.writeUTF(value.getLuogoNascita());
	}

	@Override
	public Utente deserialize(DataInput2 input, int available) throws IOException {
		return new Utente.UtenteBuilder(input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF()/*,input.readUTF(),input.readUTF(),input.readUTF()*/).setSesso(input.readUTF()).setDataNascita(input.readUTF()).setLuogoNascita(input.readUTF()).build();
	}
}
