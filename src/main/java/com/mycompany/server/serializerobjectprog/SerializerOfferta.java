package com.mycompany.server.serializerobjectprog;

import com.mycompany.shared.Offerta;
import com.mycompany.shared.Oggetto;

import java.io.IOException;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

public class SerializerOfferta implements Serializer<Offerta>, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(DataOutput2 out, Offerta value) throws IOException {
		//andiamo a serializzare i valori sul buffer in sequenza
		out.writeUTF(value.getOfferente());
		out.writeDouble(value.getImporto());
		out.writeUTF(value.oggetto.getProprietario());
		out.writeUTF(value.oggetto.getCategoria());
		out.writeUTF(value.oggetto.getNome());
		out.writeUTF(value.oggetto.getDataPubblicazione());
		out.writeUTF(value.oggetto.getDataScadenza());
		out.writeDouble(value.oggetto.getPrezzo());
		out.writeUTF(value.oggetto.getDescrizione());
		out.writeUTF(value.oggetto.getId());
		out.writeUTF(value.oggetto.getStato());
		out.writeUTF(value.getStato());
		out.writeUTF(value.getIdOfferta());

	}

	@Override
	public Offerta deserialize(DataInput2 input, int available) throws IOException {
		//andiamo a deserializzare gli attributi in sequenza di come li abbiamo serializzati
		return new Offerta(input.readUTF(),input.readDouble(),new Oggetto.OggettoBuilder(input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readDouble(),input.readUTF())
				.setId(input.readUTF()).setStato(input.readUTF()).build(),
				input.readUTF(),
				input.readUTF());
	
	}
	
}
