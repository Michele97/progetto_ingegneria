package com.mycompany.server.serializerobjectprog;

import com.mycompany.shared.Domanda;
import com.mycompany.shared.Oggetto;

import java.io.IOException;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

public class SerializerDomanda implements Serializer<Domanda>, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(DataOutput2 out, Domanda value) throws IOException {

		
		out.writeUTF(value.oggetto.getProprietario());
		out.writeUTF(value.oggetto.getCategoria());
		out.writeUTF(value.oggetto.getNome());
		out.writeUTF(value.oggetto.getDataPubblicazione());
		out.writeUTF(value.oggetto.getDataScadenza());
		out.writeDouble(value.oggetto.getPrezzo());
		out.writeUTF(value.oggetto.getDescrizione());
		out.writeUTF(value.oggetto.getId());
		out.writeUTF(value.oggetto.getStato());
		out.writeUTF(value.getQuestion());
		out.writeUTF(value.getMittente());
		out.writeUTF(value.getIdDomanda());

	}

	@Override
	public Domanda deserialize(DataInput2 input, int available) throws IOException {
		return new Domanda(new Oggetto.OggettoBuilder(input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readDouble(),input.readUTF())
						   .setId(input.readUTF()).setStato(input.readUTF()).build()
						   ,input.readUTF(),input.readUTF(),input.readUTF());
	}

}
