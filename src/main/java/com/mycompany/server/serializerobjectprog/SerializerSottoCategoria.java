package com.mycompany.server.serializerobjectprog;

import java.io.IOException;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import com.mycompany.shared.SottoCategoria;

public class SerializerSottoCategoria implements Serializer<SottoCategoria>, Serializable {

  private static final long serialVersionUID = 1L;

	@Override
	public void serialize(DataOutput2 out, SottoCategoria value) throws IOException {
		out.writeUTF(value.getNome());
    out.writeUTF(value.getCategoriaMadre());
  }

  @Override
  public SottoCategoria deserialize(DataInput2 input, int available) throws IOException {
    return new SottoCategoria(input.readUTF(),input.readUTF());
  }
}
