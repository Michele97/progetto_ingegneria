package com.mycompany.server.serializerobjectprog;

import com.mycompany.shared.Oggetto;

import java.io.IOException;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

public class SerializerOggetto implements Serializer<Oggetto>, Serializable {

	private static final long serialVersionUID = 1L;
	Oggetto o;

	@Override
	public void serialize(DataOutput2 out, Oggetto value) throws IOException {

		out.writeUTF(value.getProprietario());
		out.writeUTF(value.getCategoria());
		out.writeUTF(value.getNome());
		out.writeUTF(value.getDataPubblicazione());
		out.writeUTF(value.getDataScadenza());
		out.writeDouble(value.getPrezzo());
		out.writeUTF(value.getDescrizione());
		out.writeUTF(value.getId());
		out.writeUTF(value.getStato());

	}

	@Override
	public Oggetto deserialize(DataInput2 input, int available) throws IOException {
		o=new Oggetto.OggettoBuilder(input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readUTF(),input.readDouble(),input.readUTF())
				.setId(input.readUTF()).setStato(input.readUTF()).build();
		return o;
	}
}
