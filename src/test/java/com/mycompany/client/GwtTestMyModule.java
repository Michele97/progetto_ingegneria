package com.mycompany.client;

import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.mycompany.shared.*;
import com.mycompany.server.Admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.junit.Test;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;

public class GwtTestMyModule extends GWTTestCase {
	
	private GreetingServiceAsync greetingService;
	
	
	/******************************************************************
	 ********RICORDARSI DI INSERIRE finishTest() ALLA FINE ************
	 ********NEL METODO onSuccess() DELL'ULTIMO METODO*****************/
	
	public void initialSetUp() {
		greetingService = GWT.create(GreetingService.class);
		ServiceDefTarget target = (ServiceDefTarget) greetingService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "ByeBay/greet");
	}
	
	@Override
	public String getModuleName() {
		return "com.mycompany.MyModuleJUnit";
	}
	
	@Test
	public void testRegister() {
		initialSetUp();
	
		String username="aaaaaa";
		String username2="bbbbbb";
		DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
		
		System.out.println("\nTest per la registrazione:\n");
		greetingService.register(username, "password", "nome", "cognome", "telefono", "email", "codiceFiscale", "indirizzo", "M", format.format(new Date()), "luogoNascita", new EqualsCallback("Utente registrato con successo!"));
		greetingService.register(username2, "password", "nome", "cognome", "telefono", "email", "codiceFiscale", "indirizzo", "M", format.format(new Date()), "luogoNascita", new EqualsCallback("Utente registrato con successo!"));
		greetingService.register(username, "password", "nome", "cognome", "telefono", "email", "codiceFiscale", "indirizzo", "F", format.format(new Date()), "luogoNascita", new NotEqualsCallback("Utente registrato con successo!"));
	}
	
	@Test
	public void testLoginAdmin() {
		initialSetUp();
		Admin a=new Admin();
		System.out.println("\nTest per il login come admin:\n");
		greetingService.login(a.getUsername(),a.getPassword(), new EqualsCallback(a.getUsername()));
	}
	
	@Test
	public void testLogin() {
		initialSetUp();
		
		String username="aaaaaa";
		String usernameWrong="cccccc";
		System.out.println("\nTest per il login:\n");
		greetingService.login(username,"password", new EqualsCallback(username));
		greetingService.login(usernameWrong,"password", new NotEqualsCallback(username));
	}
	
	@Test
	public void testVendiOggetto() {
		initialSetUp();
		System.out.println("\nTest per l'inserimento di un oggetto:\n");
		DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");
		String username="aaaaaa";
		greetingService.vendiOggetto(username, "Casa", "nome", format.format(new Date()).toString(), format.format(new Date(2030,9,25)).toString(), 50.0, "Test vendiOggetto", new EqualsCallback("Oggetto inserito correttamente!"));
		greetingService.vendiOggetto(username, "Casa", "", format.format(new Date()).toString(), format.format(new Date(2030,9,25)).toString(), 50.0, "Test vendiOggetto", new NotEqualsCallback("Oggetto inserito correttamente!"));	
	}
	
	@Test
	public void testFaiOggerta() {
		initialSetUp();
		System.out.println("\nTest per l'inserimento di un offerta:\n");
		greetingService.cercaOggetto("nome", new AsyncCallback<ArrayList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Oggetto> result) {
				greetingService.faiOfferta("aaaaaa", 10000.0, result.get(0), new EqualsCallback("ok"));
				greetingService.faiOfferta("aaaaaa", 0.1, result.get(0), new NotEqualsCallback("ok"));
				finishTest();
			}
		});
	}

	@Test
	public void testFaiDomanda() {
		initialSetUp();
		System.out.println("\nTest per l'inserimento di una domanda:\n");

		String username1="aaaaaa";
		greetingService.cercaOggetto("nome", new AsyncCallback<ArrayList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Oggetto> result) {
				greetingService.faiDomanda(result.get(0), "Domanda di test",username1, new EqualsCallback("Domanda inserita corretamente!"));
				finishTest();
			}
		});
				
	}
	
	@Test
	public void testDaiRisposta() {
		initialSetUp();
		System.out.println("\nTest per l'inserimento di una risposta:\n");

		String username="aaaaaa";
		greetingService.cercaDomande(username, new AsyncCallback<ArrayList<Domanda>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Domanda> result) {
				greetingService.rispondi(result.get(0), "Risposta alla domanda di test", new EqualsCallback("Risposta inviata!"));
			}
		});		
	}
	
	@Test
	public void testInsCategory() {
		initialSetUp();
		String categoria="Categoria di test";
		String categoriaWrong=categoria;
		System.out.println("\nTest per l'inserimento di una categoria:\n");
		greetingService.insertCategory(categoria, new EqualsCallback("Categoria inserita con successo."));
		greetingService.insertCategory(categoriaWrong, new NotEqualsCallback("Categoria inserita con successo."));
	}
	
	@Test
	public void testInsSubCategory() {
		initialSetUp();
		System.out.println("\nTest per l'inserimento di una sottocategoria:\n");
		String subcategoria="SottoCategoria di test";
		String categoriaMadre="Categoria di test";

		greetingService.insertSubCategory(subcategoria,categoriaMadre, new EqualsCallback("Sottocategoria inserita con successo."));
		greetingService.insertSubCategory(subcategoria,categoriaMadre, new NotEqualsCallback("Sottocategoria inserita con successo."));
	}
	
	@Test
	public void testRenameCategory() {
		initialSetUp();
		System.out.println("\nTest per l'inserimento di una sottocategoria:\n");
		String oldcategoria="SottoCategoria di test";
		String newcategoria="Nuova Categoria di test";

		greetingService.insertSubCategory(oldcategoria,newcategoria, new EqualsCallback("Rinominazione completata con successo."));
		greetingService.insertSubCategory(oldcategoria,newcategoria, new NotEqualsCallback("Rinominazione completata con successo."));
	}
	
	@Test
	public void testRemoveObject() {
		initialSetUp();
		greetingService.cercaOggetto("nome", new AsyncCallback<ArrayList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Oggetto> result) {
				greetingService.removeObject(result.get(0), new EqualsCallback("Oggetto rimosso con successo"));
				greetingService.removeObject(result.get(0), new NotEqualsCallback("Oggetto rimosso con successo"));
				finishTest();
			}
		});
	}
	
	@Test
	public void testRemoveOffert() {
		initialSetUp();
		greetingService.allOffert(new AsyncCallback<ArrayList<Offerta>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Offerta> result) {
				greetingService.deleteOffert(result.get(0), new EqualsCallback("Offerta eliminata correttamente!"));
				greetingService.deleteOffert(result.get(0), new NotEqualsCallback("Offerta eliminata correttamente!"));
				finishTest();
			}
		});
	}
	
	@Test
	public void testRemoveQuestion() {
		initialSetUp();
		greetingService.allQuestions(new AsyncCallback<ArrayList<Domanda>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Domanda> result) {
				greetingService.removeDomanda(result.get(0), new EqualsCallback("Domanda eliminata con successo"));
				greetingService.removeDomanda(result.get(0), new NotEqualsCallback("Domanda eliminata con successo"));
				finishTest();
			}
		});
	}
	
	@Test
	public void testRemoveAnswer() {
		initialSetUp();
		greetingService.allAnswers(new AsyncCallback<ArrayList<Risposta>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Risposta> result) {
				greetingService.removeRisposta(result.get(0), new EqualsCallback("Risposta eliminata con successo"));
				greetingService.removeRisposta(result.get(0), new NotEqualsCallback("Risposta eliminata con successo"));
				finishTest();
			}
		});
	}
	
	
	
	
	private class EqualsCallback implements AsyncCallback<String>{
		private String toCompare;
		public EqualsCallback(String toCompare) {
			this.toCompare=toCompare;
		}
		
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(String result) {
			assertTrue("Deve essere true: ",result.equals(toCompare));
			finishTest();
		}
	}
	
	private class NotEqualsCallback implements AsyncCallback<String>{
		private String toCompare;
		public NotEqualsCallback(String toCompare) {
			this.toCompare=toCompare;
		}
		
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(String result) {
			assertFalse("Deve essere false: ",result.equals(toCompare));
			finishTest();
		}
	}


}
