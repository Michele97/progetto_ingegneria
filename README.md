# Progetto di ingegneria del software 2019/2020 - Informatica per il management 


## Sito di aste online

In questa repository è presente il progetto realizzato  l'insegamento di ingegneria del software  del corso di laurea triennale (informatica per il management) presso l'Università di Bologna.
Questo progetto è stato realizzato seguendo la metologia Agile Scrum.
All'interno della wiki troverete la documentazione relativa al progetto e alle scelte progettuali.


## Manuale utente
Qui troverete il [Manuale Utente]( https://docs.google.com/document/d/1SJzi1oMxGzvQ586jmLFhMaZDXCbtGbcdwZI7zAtnpPQ/edit ) .


### Uso da terminale

Comdandi da eseguire da terminale per installare ed eseguire il progetto.

```
$ git clone https://Michele97@bitbucket.org/Michele97/progetto_ingegneria.git
$ cd progetto_ingegneria/
$ mvn war:exploded
$ mvn gwt:run
```

### Autori


| Nome e Cognome   | Indirizzo mail                   | Matricola |
|------------------|----------------------------------|-----------|
| Omar Rida        | omar.rida@studio.unibo.it        | 838871    |
| Emanuele Fazzini | emanuele.fazzini@studio.unibo.it | 825065    |
| Michele Bonini   | michele.bonini2@studio.unibo.it  | 840831    |

